package okielegym.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OkieleGymApplication {
    public static void main(String[] args) {
        SpringApplication.run(OkieleGymApplication.class, args);
    }
}
