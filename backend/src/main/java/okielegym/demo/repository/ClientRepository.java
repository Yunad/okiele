package okielegym.demo.repository;

import okielegym.demo.persistence.model.Client;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends UserRepository<Client, Long> {
    List<Client> getAllByName(String name);
}
