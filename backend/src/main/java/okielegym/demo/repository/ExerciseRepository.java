package okielegym.demo.repository;

import okielegym.demo.persistence.model.Exercise;
import okielegym.demo.persistence.model.type.ExerciseType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, Long> {
    Exercise findExerciseByExerciseType(ExerciseType exerciseType);
}
