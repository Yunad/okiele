package okielegym.demo.repository;

import okielegym.demo.persistence.model.Gym;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GymRepository extends JpaRepository<Gym, Long> {
    Gym findGymByName(String gymName);
}
