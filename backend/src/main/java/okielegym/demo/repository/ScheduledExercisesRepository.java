package okielegym.demo.repository;

import okielegym.demo.persistence.model.Room;
import okielegym.demo.persistence.model.ScheduledExercise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.DayOfWeek;
import java.util.List;

@Repository
public interface ScheduledExercisesRepository extends JpaRepository<ScheduledExercise, Long> {
    List<ScheduledExercise> getByDayOfWeek(DayOfWeek dayOfWeek);

    List<ScheduledExercise> getAllByDayOfWeekAndRoom(DayOfWeek dayOfWeek, Room room);
}
