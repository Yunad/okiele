package okielegym.demo.repository;

import okielegym.demo.persistence.model.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface UserRepository<T, ID> extends JpaRepository<T, ID> {
    T findUserByEmail(String email);

    List<T> findUsersByUserType(UserType userType);

    void deleteByEmail(String email);

    @Query("SELECT DISTINCT u.userType FROM USERS u")
    List<UserType> getUserTypes();
}
