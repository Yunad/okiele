package okielegym.demo.repository;

import okielegym.demo.persistence.model.Information;
import okielegym.demo.persistence.model.type.InformationTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InformationRepository extends JpaRepository<Information, Long> {
    List<InformationTypes> getAllByInformationTypes(InformationTypes informationTypes);
}
