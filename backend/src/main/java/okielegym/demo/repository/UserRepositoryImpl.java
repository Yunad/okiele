package okielegym.demo.repository;

import okielegym.demo.persistence.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepositoryImpl extends UserRepository<User, Long> {
}
