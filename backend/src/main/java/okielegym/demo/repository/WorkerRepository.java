package okielegym.demo.repository;

import okielegym.demo.persistence.model.Worker;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkerRepository extends UserRepository<Worker, Long> {
}
