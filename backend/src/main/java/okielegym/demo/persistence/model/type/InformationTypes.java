package okielegym.demo.persistence.model.type;

public enum InformationTypes {
    INFO, IMPORTANT, BLOG, PRIVATE
}
