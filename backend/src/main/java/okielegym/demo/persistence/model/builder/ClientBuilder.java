package okielegym.demo.persistence.model.builder;

import okielegym.demo.persistence.model.Client;
import okielegym.demo.persistence.model.Schedule;

import java.util.HashSet;
import java.util.Set;

public class ClientBuilder extends UserBuilder<ClientBuilder> {
    private boolean blocked;
    private boolean agreedForSms;
    private boolean agreedForEmail;
    private int absenceCounter;
    private Set<Schedule> clientSchedules = new HashSet<>();

    public Client build() {
        return new Client(this);
    }

    public ClientBuilder isBlocked(boolean isBlocked) {
        this.blocked = isBlocked;
        return getThis();
    }

    public ClientBuilder isAgreedForSms(boolean isAgreedForSms) {
        this.agreedForSms = isAgreedForSms;
        return getThis();
    }

    public ClientBuilder isAgreedForEmail(boolean isAgreedForEmail) {
        this.agreedForEmail = isAgreedForEmail;
        return getThis();
    }

    public ClientBuilder setSchedules(Set<Schedule> schedules) {
        this.clientSchedules = schedules;
        return getThis();
    }

    @Override
    protected ClientBuilder getThis() {
        return this;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public boolean isAgreedForSms() {
        return agreedForSms;
    }

    public boolean isAgreedForEmail() {
        return agreedForEmail;
    }

    public int getAbsenceCounter() {
        return absenceCounter;
    }

    public ClientBuilder setAbsenceCounter(int absenceCounter) {
        this.absenceCounter = absenceCounter;
        return getThis();
    }

    public Set<Schedule> getClientSchedules() {
        return clientSchedules;
    }
}
