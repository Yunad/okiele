package okielegym.demo.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.sql.Date;
import java.util.Objects;

@Entity(name = "EVENTS")
public class Event extends Schedule {
    @Column(name = "EVENT_DATE", nullable = false)
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return super.toString()
                .concat("Event{" +
                        "date=" + date +
                        '}');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        Event event = (Event) o;
        return date.equals(event.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }
}
