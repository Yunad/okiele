package okielegym.demo.persistence.model;

import okielegym.demo.persistence.model.type.ExerciseType;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity(name = "EXERCISES")
public class Exercise {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    @Enumerated(EnumType.STRING)
    private ExerciseType exerciseType;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Room room;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ExerciseType getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(ExerciseType exerciseType) {
        this.exerciseType = exerciseType;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room roomClasses) {
        this.room = roomClasses;
    }

    @Override
    public String toString() {
        return "Exercise{" +
                "id=" + id +
                ", gymClassType=" + exerciseType +
                ", roomClasses=" + room +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Exercise)) return false;
        Exercise exercise = (Exercise) o;
        return id == exercise.id &&
                exerciseType == exercise.exerciseType &&
                Objects.equals(room, exercise.room);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, exerciseType, room);
    }
}
