package okielegym.demo.persistence.model;

import okielegym.demo.persistence.model.type.RoomTypes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "ROOM_TYPES")
public class RoomType {
    @Id
    @GeneratedValue
    private long id;
    @Column(name = "ROOM_TYPE_NAMES",
            nullable = false)
    private RoomTypes roomTypeName;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Gym> gyms;

    public RoomType() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RoomTypes getRoomTypeName() {
        return roomTypeName;
    }

    public void setRoomTypeName(RoomTypes roomTypeName) {
        this.roomTypeName = roomTypeName;
    }

    public List<Gym> getGyms() {
        return gyms;
    }

    public void setGyms(List<Gym> gyms) {
        this.gyms = gyms;
    }
}
