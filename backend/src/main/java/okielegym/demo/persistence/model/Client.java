package okielegym.demo.persistence.model;

import okielegym.demo.persistence.model.builder.ClientBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity(name = "CLIENTS")
public class Client extends User {
    @Column(name = "IS_AGREED_FOR_SMS",
            nullable = false)
    private boolean agreedForSms;
    @Column(name = "IS_AGREED_FOR_EMAIL",
            nullable = false)
    private boolean agreedForEmail;
    @Column(name = "ABSENCE_COUNTER")
    private int absenceCounter;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "CLIENTS_EXERCISES",
            joinColumns = @JoinColumn(name = "CLIENTS_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "SCHEDULE_ID", referencedColumnName = "ID"))
    private Set<Schedule> scheduleExercise = new HashSet<>();

    public Client() {
    }

    public Client(ClientBuilder clientBuilder) {
        super(clientBuilder);
        this.agreedForEmail = clientBuilder.isAgreedForEmail();
        this.agreedForSms = clientBuilder.isAgreedForSms();
        this.absenceCounter = clientBuilder.getAbsenceCounter();
        this.scheduleExercise = clientBuilder.getClientSchedules();
    }

    public boolean isAgreedForSms() {
        return agreedForSms;
    }

    public void setAgreedForSms(boolean agreedForSms) {
        this.agreedForSms = agreedForSms;
    }

    public boolean isAgreedForEmail() {
        return agreedForEmail;
    }

    public void setAgreedForEmail(boolean agreedForEmail) {
        this.agreedForEmail = agreedForEmail;
    }

    public int getAbsenceCounter() {
        return absenceCounter;
    }

    public void setAbsenceCounter(int absenceCounter) {
        this.absenceCounter = absenceCounter;
    }

    public Set<Schedule> getScheduleExercise() {
        return scheduleExercise;
    }

    public void setScheduleExercise(Set<Schedule> scheduleExercise) {
        this.scheduleExercise = scheduleExercise;
    }

    public void addSchedule(ScheduledExercise scheduleExercise) {
        getScheduleExercise().add(scheduleExercise);
    }

    @Override
    public String toString() {
        return super.toString()
                .concat("Client{" +
                        ", isAgreedForSms=" + agreedForSms +
                        ", isAgreedForEmail=" + agreedForEmail +
                        ", absenceCounter=" + absenceCounter +
                        ", scheduleExercise=" + scheduleExercise +
                        '}');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        if (!super.equals(o)) return false;
        Client client = (Client) o;
        return agreedForSms == client.agreedForSms &&
                agreedForEmail == client.agreedForEmail &&
                absenceCounter == client.absenceCounter;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), agreedForSms, agreedForEmail, absenceCounter);
    }
}
