package okielegym.demo.persistence.model.type;

public enum RoomTypes {
    FITNESS, BODY_MENTAL, JUMPIT
}
