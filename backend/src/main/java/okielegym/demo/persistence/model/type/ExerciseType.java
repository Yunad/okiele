package okielegym.demo.persistence.model.type;

public enum ExerciseType {
    CARDIO, WEIGHTS, HEALTHY, DYNAMIC, STATIC
}
