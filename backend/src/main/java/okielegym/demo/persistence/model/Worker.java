package okielegym.demo.persistence.model;

import okielegym.demo.persistence.model.builder.WorkerBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity(name = "WORKERS")
public class Worker extends User {
    @Column(name = "SALARY",
            nullable = false)
    private BigDecimal salary;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<WorkerSchedule> workerSchedule = new HashSet<>();
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ScheduledExercise scheduledExercise;

    public Worker() {
    }

    public Worker(WorkerBuilder builder) {
        super(builder);
        this.salary = builder.getSalary();
        this.workerSchedule = builder.getWorkerSchedule();
        this.scheduledExercise = builder.getScheduleExercise();
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Set<WorkerSchedule> getWorkerSchedule() {
        return workerSchedule;
    }

    public void setWorkerSchedule(Set<WorkerSchedule> workersCalendar) {
        this.workerSchedule = workersCalendar;
    }

    public void workerSchedule(WorkerSchedule workerSchedule) {
        getWorkerSchedule().add(workerSchedule);
    }

    public ScheduledExercise getScheduledExercise() {
        return scheduledExercise;
    }

    public void setScheduledExercise(ScheduledExercise scheduledExercise) {
        this.scheduledExercise = scheduledExercise;
    }

    public void addWorkerSchedule(WorkerSchedule workerSchedule) {
        getWorkerSchedule().add(workerSchedule);
    }

    @Override
    public String toString() {
        return "Worker{" +
                "id:" + getId() +
                ", salary=" + salary +
                ", workerSchedule=" + workerSchedule +
                ", schedule=" + scheduledExercise +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Worker)) return false;
        if (!super.equals(o)) return false;
        Worker worker = (Worker) o;
        return salary.equals(worker.salary) &&
                scheduledExercise.equals(worker.scheduledExercise);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), salary, scheduledExercise);
    }
}
