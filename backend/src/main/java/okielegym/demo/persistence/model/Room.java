package okielegym.demo.persistence.model;

import okielegym.demo.persistence.model.type.RoomTypes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "ROOMS")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "NAMES",
            nullable = false)
    private String roomName;
    @Column(name = "ROOM_TYPE",
            nullable = false)
    private RoomTypes roomType;
    @Column(name = "CAPACITY",
            nullable = false)
    private long capacity;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Exercise> gymClasses = new HashSet<>();
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Schedule> schedules = new HashSet<>();

    public Room() {
    }

    public Room(RoomTypes roomType, int roomCapacity, String roomName) {
        this.roomType = roomType;
        this.capacity = roomCapacity;
        this.roomName = roomName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoomTypes getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomTypes roomType) {
        this.roomType = roomType;
    }

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Set<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(Set<Schedule> schedules) {
        this.schedules = schedules;
    }

    public Set<Exercise> getGymClasses() {
        return gymClasses;
    }

    public void setGymClasses(Set<Exercise> gymClasses) {
        this.gymClasses = gymClasses;
    }

    public void addSchedule(Schedule schedule) {
        getSchedules().add(schedule);
    }
}
