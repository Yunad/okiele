package okielegym.demo.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.DayOfWeek;
import java.util.Objects;

@Entity(name = "SCHEDULED_EXERCISES")
public class ScheduledExercise extends Schedule {
    @Column(name = "DAY",
            nullable = false)
    private DayOfWeek dayOfWeek;

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Override
    public String toString() {
        return super.toString()
                .concat("ScheduledExercise{" +
                        "dayOfWeek=" + dayOfWeek +
                        '}');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScheduledExercise)) return false;
        if (!super.equals(o)) return false;
        ScheduledExercise that = (ScheduledExercise) o;
        return dayOfWeek == that.dayOfWeek;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), dayOfWeek);
    }
}
