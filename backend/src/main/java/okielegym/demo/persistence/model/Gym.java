package okielegym.demo.persistence.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import java.sql.Time;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity(name = "GYMS")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "className")
@SequenceGenerator(name = "sequence")
//TODO add layer which will handle LAZY fetch initialization.
public class Gym {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
    private Long id;
    @Column(length = 40,
            nullable = false)
    private String name;
    @Column(length = 50,
            nullable = false)
    private String address;
    @Column(name = "IS_PARKING_LOT",
            nullable = false)
    private boolean parkingLot;
    @Column(name = "IS_SAUNA",
            nullable = false)
    private boolean sauna;
    @Column(name = "STARTING_HOURS",
            nullable = false)
    private Time startingHours;
    @Column(name = "ENDING_HOURS",
            nullable = false)
    private Time endingHours;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Information> informationList = new HashSet<>();
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<ScheduledExercise> scheduledExercises = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isParkingLot() {
        return parkingLot;
    }

    public void setParkingLot(boolean parkingLot) {
        this.parkingLot = parkingLot;
    }

    public boolean isSauna() {
        return sauna;
    }

    public void setSauna(boolean sauna) {
        this.sauna = sauna;
    }

    public Time getStartingHours() {
        return startingHours;
    }

    public void setStartingHours(Time startingHours) {
        this.startingHours = startingHours;
    }

    public Time getEndingHours() {
        return endingHours;
    }

    public void setEndingHours(Time endingHours) {
        this.endingHours = endingHours;
    }

    public Set<Information> getInformationList() {
        return informationList;
    }

    public void setInformationList(Set<Information> informations) {
        this.informationList = informations;
    }

    public Set<ScheduledExercise> getScheduledExercises() {
        return scheduledExercises;
    }

    public void setScheduledExercises(Set<ScheduledExercise> scheduledExercises) {
        this.scheduledExercises = scheduledExercises;
    }

    public void addScheduledExercise(ScheduledExercise scheduledExercise) {
        getScheduledExercises().add(scheduledExercise);
    }

    public void addInformation(Information information) {
        getInformationList().add(information);
    }

    @Override
    public String toString() {
        return "Gym{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", isParkingLot=" + parkingLot +
                ", isSauna=" + sauna +
                ", startingHours=" + startingHours +
                ", endingHours=" + endingHours +
                ", informationList=" + informationList +
                ", scheduledExercises=" + scheduledExercises +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gym)) return false;
        Gym gym = (Gym) o;
        return id.equals(gym.id) &&
                name.equals(gym.name) &&
                address.equals(gym.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, address);
    }
}
