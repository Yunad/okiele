package okielegym.demo.persistence.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.sql.Time;
import java.util.Date;
import java.util.Objects;

@Entity(name = "WORKER_SCHEDULE")
public class WorkerSchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    @Column(name = "DATE",
            nullable = false)
    private Date date;
    @Column(name = "STARTING_TIME",
            nullable = false)
    private Time startingTime;
    @Column(name = "ENDING_TIME",
            nullable = false)
    private Time endingTime;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Worker worker;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(Time startingTime) {
        this.startingTime = startingTime;
    }

    public Time getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(Time endingTime) {
        this.endingTime = endingTime;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WorkerSchedule)) return false;
        WorkerSchedule that = (WorkerSchedule) o;
        return Objects.equals(date, that.date) &&
                Objects.equals(startingTime, that.startingTime) &&
                Objects.equals(endingTime, that.endingTime) &&
                Objects.equals(worker, that.worker);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, startingTime, endingTime);
    }

    @Override
    public String toString() {
        return "WorkerCalendar{" +
                "date=" + date +
                ", startingTime=" + startingTime +
                ", endingTime=" + endingTime +
                '}';
    }
}
