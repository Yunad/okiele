package okielegym.demo.persistence.model.builder;

import okielegym.demo.persistence.model.ScheduledExercise;
import okielegym.demo.persistence.model.Worker;
import okielegym.demo.persistence.model.WorkerSchedule;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class WorkerBuilder extends UserBuilder<WorkerBuilder> {
    private BigDecimal salary;
    private Set<WorkerSchedule> workerSchedule = new HashSet<>();
    private ScheduledExercise scheduledExercise;

    public Worker build() {
        return new Worker(this);
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public WorkerBuilder setSalary(BigDecimal salary) {
        this.salary = salary;
        return getThis();
    }

    public Set<WorkerSchedule> getWorkerSchedule() {
        return workerSchedule;
    }

    public WorkerBuilder setWorkerSchedule(WorkerSchedule... workerSchedule) {
        this.workerSchedule.addAll(Arrays.asList(workerSchedule));
        return getThis();
    }

    @Override
    protected WorkerBuilder getThis() {
        return this;
    }

    public ScheduledExercise getScheduleExercise() {
        return this.scheduledExercise;
    }

    public WorkerBuilder setSchedule(ScheduledExercise scheduledExercise) {
        this.scheduledExercise = scheduledExercise;
        return getThis();
    }
}
