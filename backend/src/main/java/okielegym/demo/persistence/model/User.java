package okielegym.demo.persistence.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import okielegym.demo.persistence.model.builder.UserBuilder;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Arrays;
import java.util.Objects;

@Entity(name = "USERS")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "className")
@Inheritance(strategy = InheritanceType.JOINED)
@SequenceGenerator(name = "sequence")
public abstract class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
    private Long id;
    @Column(name = "NAME",
            nullable = false,
            length = 50)
    private String name;
    @Column(name = "LAST_NAME",
            nullable = false,
            length = 50)
    private String lastName;
    @Column(name = "ADDRESS",
            nullable = false,
            length = 60)
    private String address;
    @Column(name = "PICTURE")
    private Byte[] picture;
    @Column(name = "EMAIL",
            nullable = false,
            length = 50)
    @Email
    private String email;
    @Column(name = "PHONE")
    private String phone;//FIXME add validation for the PHONE, matching front side.
    @Column(name = "PASSWORD",
            nullable = false)
    private String password;
    @ManyToOne(fetch = FetchType.EAGER)
    private UserType userType;
    @Column(name = "IS_BLOCKED",
            nullable = false)
    private boolean blocked;

    public User() {
    }

    protected User(UserBuilder builder) {
        this.email = builder.getEmail();
        this.name = builder.getName();
        this.lastName = builder.getLastName();
        this.picture = builder.getPicture();
        this.phone = builder.getPhone();
        this.password = builder.getPassword();
        this.userType = builder.getUserType();
        this.address = builder.getAddress();
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Byte[] getPicture() {
        return picture;
    }

    public void setPicture(Byte[] picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getUserType() {
        return userType;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public void setUserType(UserType userType) {

        this.userType = userType;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", picture=" + Arrays.toString(picture) +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", userType=" + userType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return id.equals(user.id) &&
                name.equals(user.name) &&
                lastName.equals(user.lastName) &&
                address.equals(user.address) &&
                email.equals(user.email) &&
                phone.equals(user.phone) &&
                password.equals(user.password) &&
                userType == user.userType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastName, address, email, userType);
    }
}
