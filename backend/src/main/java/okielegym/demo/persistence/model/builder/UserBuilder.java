package okielegym.demo.persistence.model.builder;

import okielegym.demo.persistence.model.UserType;

public abstract class UserBuilder<T extends UserBuilder<T>> {
    private String name;
    private String lastName;
    private String address;
    private Byte[] picture;
    private String email;
    private String phone;
    private String password;
    private UserType userType;

    public UserBuilder() {
    }

    protected abstract T getThis();

    public String getName() {
        return name;
    }

    public T setName(String name) {
        this.name = name;
        return getThis();
    }

    public String getLastName() {
        return lastName;
    }

    public T setLastName(String lastName) {
        this.lastName = lastName;
        return getThis();
    }

    public String getAddress() {
        return address;
    }

    public T setAddress(String address) {
        this.address = address;
        return getThis();
    }

    public Byte[] getPicture() {
        return picture;
    }

    public T setPicture(Byte[] picture) {
        this.picture = picture;
        return getThis();
    }

    public String getEmail() {
        return email;
    }

    public T setEmail(String email) {
        this.email = email;
        return getThis();
    }

    public String getPhone() {
        return phone;
    }

    public T setPhone(String phone) {
        this.phone = phone;
        return getThis();
    }

    public String getPassword() {
        return password;
    }

    public T setPassword(String password) {
        this.password = password;
        return getThis();
    }

    public UserType getUserType() {
        return userType;
    }

    public T setUserType(UserType userType) {
        this.userType = userType;
        return getThis();
    }
}
