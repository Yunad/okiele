package okielegym.demo.persistence.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "USER_TYPES")
public class UserType {
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private final List<User> users = new ArrayList<>();
    @Id
    private long id;
    @Column(name = "TYPE_NAME",
            nullable = false, length = 30)
    private String typeName;

    public UserType() {
    }

    public UserType(String typeName) {
        this.typeName = typeName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "UserType{" +
                "id=" + id +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
