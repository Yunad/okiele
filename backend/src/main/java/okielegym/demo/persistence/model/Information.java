package okielegym.demo.persistence.model;

import okielegym.demo.persistence.model.type.InformationTypes;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;

@Entity(name = "INFORMATIONS")
@SequenceGenerator(name = "sequence")
public class Information {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
    private Long id;
    @Column(name = "INFORMATION_TYPE",
            nullable = false)
    @Enumerated(EnumType.STRING)
    private InformationTypes informationTypes;
    @Column(name = "MESSAGES",
            nullable = false)
    private String message;
    @Column(name = "IMAGES")
    @Lob//FIXME https://stackoverflow.com/questions/50363639/how-spring-boot-jpahibernate-saves-images
    private Byte[] image;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Gym gym;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InformationTypes getInformationTypes() {
        return informationTypes;
    }

    public void setInformationTypes(InformationTypes informationTypes) {
        this.informationTypes = informationTypes;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public Gym getGym() {
        return gym;
    }

    public void setGym(Gym gym) {
        this.gym = gym;
    }

    @Override
    public String toString() {
        return "Information{" +
                "id=" + id +
                ", informationType=" + informationTypes +
                ", message='" + message + '\'' +
                ", image=" + Arrays.toString(image) +
                ", gym=" + gym +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Information)) return false;
        Information that = (Information) o;
        return id.equals(that.id) &&
                informationTypes == that.informationTypes &&
                message.equals(that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, informationTypes, message);
    }
}
