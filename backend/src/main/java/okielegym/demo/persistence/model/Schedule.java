package okielegym.demo.persistence.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity(name = "Schedule")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "className")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "TIME",
            nullable = false)
    private Time time;
    @Column(name = "TIMESTAMP",
            nullable = false)
    private Timestamp timestamp;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ROOM")
    private Room room;
    @JoinColumn(name = "FK_WORKER")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Worker worker;
    @ManyToMany(mappedBy = "scheduleExercise", fetch = FetchType.EAGER)
    private Set<Client> clients = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    public Set<Client> getClients() {
        return clients;
    }

    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }

    public void addClient(Client client) {
        getClients().add(client);
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", time=" + time +
                ", timestamp=" + timestamp +
                ", room=" + room +
                ", worker=" + worker +
                ", clients=" + clients +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Schedule)) return false;
        Schedule schedule = (Schedule) o;
        return id.equals(schedule.id) &&
                time.equals(schedule.time) &&
                timestamp.equals(schedule.timestamp) &&
                room.equals(schedule.room) &&
                worker.equals(schedule.worker);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time, timestamp, room, worker);
    }
}
