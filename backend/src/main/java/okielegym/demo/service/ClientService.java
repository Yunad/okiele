package okielegym.demo.service;

import okielegym.demo.persistence.model.Client;
import okielegym.demo.repository.ClientRepository;
import okielegym.demo.service.exceptions.ServicesException;
import okielegym.demo.service.utils.ServiceUtils;
import org.apache.commons.collections4.IterableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class ClientService extends UserService<Client> {
    public static final Logger LOGGER = LoggerFactory.getLogger(ClientService.class);
    private final ClientRepository clientRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public ClientService(ClientRepository clientRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.clientRepository = clientRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public List<Client> getAgreedSmsClients(boolean isAgreed) {
        List<Client> clients = IterableUtils.toList(clientRepository.findAll());
        return clients
                .stream()
                .filter(client -> client.isAgreedForSms() == isAgreed)
                .collect(Collectors.toList());
    }

    public Client updateClientSmsAgreement(String clientEmail, boolean isAgreed) throws ServicesException {
        LOGGER.info("Updating client`s: {} agreement for SMS.", clientEmail);
        Client userByEmail = clientRepository.findUserByEmail(clientEmail);
        if (userByEmail == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(clientEmail));
        }
        userByEmail.setAgreedForSms(isAgreed);
        return clientRepository.save(userByEmail);
    }

    public List<Client> getAgreedEmailClients(boolean isAgreed) {
        List<Client> clients = IterableUtils.toList(clientRepository.findAll());
        return clients
                .stream()
                .filter(client -> client.isAgreedForEmail() == isAgreed)
                .collect(Collectors.toList());
    }

    public Client updateClientEmailAgreement(String clientEmail, boolean isAgreed) throws ServicesException {
        LOGGER.info("Updating client`s: {} agreement for emails.", clientEmail);
        Client userByEmail = clientRepository.findUserByEmail(clientEmail);
        if (userByEmail == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(clientEmail));
        }
        userByEmail.setAgreedForEmail(isAgreed);
        return clientRepository.save(userByEmail);
    }

    @Override
    protected ClientRepository getRepository() {
        return this.clientRepository;
    }

    @Override
    protected BCryptPasswordEncoder getBCryptPassword() {
        return this.bCryptPasswordEncoder;
    }
}
