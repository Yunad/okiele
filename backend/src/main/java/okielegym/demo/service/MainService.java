package okielegym.demo.service;

import okielegym.demo.service.exceptions.ServicesException;
import org.springframework.transaction.annotation.Transactional;

public interface MainService<T> {
    @Transactional
    T create(T o) throws ServicesException;

    @Transactional
    T update(T o) throws ServicesException;

    @Transactional
    void delete(T o) throws ServicesException;

    Iterable<T> getAll();
}
