package okielegym.demo.service;

import okielegym.demo.persistence.model.Worker;
import okielegym.demo.persistence.model.WorkerSchedule;
import okielegym.demo.repository.WorkerRepository;
import okielegym.demo.service.exceptions.ServicesException;
import okielegym.demo.service.utils.ServiceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
public class WorkerService extends UserService<Worker> {
    public static final Logger LOGGER = LoggerFactory.getLogger(WorkerService.class);
    private WorkerRepository workerRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public WorkerService(WorkerRepository workerRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.workerRepository = workerRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public Set<WorkerSchedule> getWorkerCalendarList(String workerEmail) throws ServicesException {
        Worker userByEmail = workerRepository.findUserByEmail(workerEmail);
        if (userByEmail == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(workerEmail));
        }
        return userByEmail.getWorkerSchedule();
    }

    @Transactional
    public Worker addScheduleToWorker(String workerEmail, WorkerSchedule workerSchedule) throws ServicesException {
        Worker userByEmail = workerRepository.findUserByEmail(workerEmail);
        if (userByEmail == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(workerEmail));
        }
        userByEmail.getWorkerSchedule().add(workerSchedule);
        LOGGER.info("Adding schedule: {} to worker: {}.", workerSchedule, workerEmail);
        return workerRepository.save(userByEmail);
    }

    @Transactional
    public Worker updateWorkerCalendar(String workerEmail, WorkerSchedule workerSchedule) throws ServicesException {
        Worker userByEmail = workerRepository.findUserByEmail(workerEmail);
        if (userByEmail == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(workerEmail));
        }
        WorkerSchedule workerScheduleToUpdate = getWorkerCalendarToUpdate(workerSchedule, userByEmail);
        userByEmail.getWorkerSchedule().add(workerScheduleToUpdate);
        LOGGER.info("Updating schedule: {}", workerScheduleToUpdate);
        return workerRepository.save(userByEmail);
    }

    private WorkerSchedule getWorkerCalendarToUpdate(WorkerSchedule workerSchedule, Worker userByEmail) {
        Set<WorkerSchedule> workersCalendar = userByEmail.getWorkerSchedule();
        return workersCalendar
                .stream()
                .filter(calendar -> calendar.getId() == workerSchedule.getId())
                .findAny()
                .orElseThrow();
    }

    @Override
    protected WorkerRepository getRepository() {
        return this.workerRepository;
    }

    @Override
    protected BCryptPasswordEncoder getBCryptPassword() {
        return this.bCryptPasswordEncoder;
    }
}
