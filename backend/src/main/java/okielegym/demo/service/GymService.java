package okielegym.demo.service;

import okielegym.demo.persistence.model.Gym;
import okielegym.demo.persistence.model.Information;
import okielegym.demo.persistence.model.ScheduledExercise;
import okielegym.demo.repository.GymRepository;
import okielegym.demo.service.exceptions.ServicesException;
import okielegym.demo.service.utils.ServiceUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
public class GymService implements MainService<Gym> {
    private static final Logger LOGGER = LogManager.getLogger(GymService.class);
    private final GymRepository gymRepository;
    private final InformationService informationService;
    private final ScheduledExerciseService scheduledExerciseService;

    public GymService(GymRepository gymRepository, InformationService informationService, ScheduledExerciseService scheduledExerciseService) {
        this.gymRepository = gymRepository;
        this.informationService = informationService;
        this.scheduledExerciseService = scheduledExerciseService;
    }

    @Override
    public Gym create(Gym gym) throws ServicesException {
        LOGGER.info("Saving {} to database", gym.getName());
        Gym gymIfExists = getGymByName(gym.getName());
        if (gymIfExists != null) {
            throw new ServicesException(ServiceUtils.getEntityInDatabaseMessage(gym.getName()));
        }
        return gymRepository.save(gym);
    }

    @Override
    public Gym update(Gym updatedGym) throws ServicesException {
        Gym entityInDatabase = getGymByName(updatedGym.getName());
        if (entityInDatabase == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(updatedGym.getName()));
        }
        BeanUtils.copyProperties(updatedGym, entityInDatabase);
        LOGGER.info("Updating {} to database.", entityInDatabase);
        return gymRepository.save(entityInDatabase);
    }

    @Override
    public void delete(Gym gym) throws ServicesException {
        LOGGER.info("Deleting {} from database.", gym.getName());
        Gym gymIfExists = getGymByName(gym.getName());
        if (gymIfExists == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(gym.getName()));
        }
        gymRepository.delete(gym);
    }

    @Override
    public List<Gym> getAll() {
        return IterableUtils.toList(gymRepository.findAll());
    }

    @Transactional
    public void deleteGymById(long id) throws ServicesException {
        Gym gymById = getGymById(id);
        LOGGER.info("Deleting {} from database.", gymById.getName());
        gymRepository.deleteById(id);
    }


    @Transactional
    public Gym deleteGymInformation(String gymName, Information information) throws ServicesException {
        LOGGER.info("Deleting information {}, from {}", information.getInformationTypes(), gymName);
        Gym gymFromDatabase = getGymByName(gymName);
        if (gymFromDatabase == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(gymName));
        }
        gymFromDatabase.getInformationList().remove(information);

        return gymRepository.save(gymFromDatabase);
    }

    public Gym createGymInformation(String gymName, Information information) throws ServicesException {
        Gym gymFromDatabase = getGymByName(gymName);
        if (gymFromDatabase == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(gymName));
        }
        gymFromDatabase.addInformation(information);
        return gymRepository.save(gymFromDatabase);
    }

    @Transactional
    public Gym updateGymInformation(String gymName, Information information) throws ServicesException {
        Gym gym = updateWantedGymInformation(gymName, information);
        return gymRepository.save(gym);
    }

    public Set<Information> getGymInformationList(String gymName) throws ServicesException {
        LOGGER.info("Getting information list for a gym: {}", gymName);
        Gym gymFromDatabase = getGymByName(gymName);
        if (gymFromDatabase == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(gymName));
        }
        return gymFromDatabase.getInformationList();
    }

    public Gym createScheduledExercise(String gymName, ScheduledExercise scheduledExercise) throws ServicesException {
        LOGGER.info("Add calendar to gym: {}", gymName);
        Gym gymFromDatabase = getGymByName(gymName);
        if (gymFromDatabase == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(gymName));
        }
        gymFromDatabase.addScheduledExercise(scheduledExercise);
        return gymRepository.save(gymFromDatabase);
    }

    @Transactional
    public Gym deleteScheduledExercise(String gymName, ScheduledExercise scheduledExercise) throws ServicesException {
        LOGGER.info("Deleting gym calendar, from gym: {}", gymName);
        Gym gymFromDatabase = getGymByName(gymName);
        if (gymFromDatabase == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(gymName));
        }
        ScheduledExercise scheduleToUpdate = getScheduleToUpdate(scheduledExercise, gymFromDatabase);
        gymFromDatabase.getScheduledExercises().remove(scheduleToUpdate);
        return gymRepository.save(gymFromDatabase);
    }

    public Set<ScheduledExercise> getGymScheduledExerciseList(String gymName) throws ServicesException {
        LOGGER.info("Getting list of Calendar for gym: {}", gymName);
        Gym gymFromDatabase = getGymByName(gymName);
        if (gymFromDatabase == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(gymName));
        }
        return gymFromDatabase.getScheduledExercises();
    }

    @Transactional
    public Gym updateScheduledExercise(String gymName, ScheduledExercise scheduledExercise) throws ServicesException {
        Gym gym = updateWantedScheduledExercise(gymName, scheduledExercise);
        return gymRepository.save(gym);
    }

    public Gym getGymByName(String gymName) {
        return gymRepository.findGymByName(gymName);
    }

    private Information getInformationToUpdate(Information information, Gym gym) {
        Set<Information> gymInformationList = gym.getInformationList();
        return gymInformationList
                .stream()
                .filter(gymInformation -> gymInformation.getId().equals(information.getId()))
                .findAny()
                .orElseThrow();
    }

    private ScheduledExercise getScheduleToUpdate(ScheduledExercise scheduledExercise, Gym gym) {
        Set<ScheduledExercise> gymsCalendar = gym.getScheduledExercises();
        return gymsCalendar
                .stream()
                .filter(calendar -> calendar.getId().equals(scheduledExercise.getId()))
                .findAny()
                .orElseThrow();
    }

    private Gym getGymById(long id) throws ServicesException {
        return gymRepository.findById(id)
                .orElseThrow(ServicesException::new);
    }

    private Gym updateWantedGymInformation(String gymName, Information information) throws ServicesException {
        Gym gymFromDatabase = getGymByName(gymName);
        if (gymFromDatabase == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(gymName));
        }
        Information informationToUpdate = getInformationToUpdate(information, gymFromDatabase);
        gymFromDatabase.getInformationList().remove(informationToUpdate);
        informationService.refresh();
        BeanUtils.copyProperties(information, informationToUpdate);
        gymFromDatabase.addInformation(informationToUpdate);
        return gymFromDatabase;
    }

    private Gym updateWantedScheduledExercise(String gymName, ScheduledExercise scheduledExercise) throws ServicesException {
        Gym gymByName = getGymByName(gymName);
        if (gymByName == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(gymName));
        }
        ScheduledExercise scheduledExerciseToUpdate = getScheduleToUpdate(scheduledExercise, gymByName);
        gymByName.getScheduledExercises().remove(scheduledExerciseToUpdate);
        gymRepository.save(gymByName);
        scheduledExerciseService.refresh();
        BeanUtils.copyProperties(scheduledExercise, scheduledExerciseToUpdate);
        gymByName.addScheduledExercise(scheduledExercise);
        return gymByName;
    }
}
