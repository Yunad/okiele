package okielegym.demo.service;

import okielegym.demo.persistence.model.Room;
import okielegym.demo.repository.RoomRepository;
import okielegym.demo.service.exceptions.ServicesException;
import okielegym.demo.service.utils.ServiceUtils;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService implements MainService<Room> {
    private final RoomRepository roomRepository;

    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public Room create(Room roomToCreate) {
        return roomRepository.save(roomToCreate);
    }

    @Override
    public Room update(Room room) throws ServicesException {
        Room roomIfExists = getRoomIfExists(room.getId());
        if (roomIfExists == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(room.getRoomType().name()));
        }
        BeanUtils.copyProperties(room, roomIfExists);
        return roomRepository.save(roomIfExists);
    }

    @Override
    public void delete(Room room) throws ServicesException {
        Room roomIfExists = getRoomIfExists(room.getId());
        if (roomIfExists == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(room.getRoomType().name()));
        }
        roomRepository.delete(room);
    }

    @Override
    public List<Room> getAll() {
        return IterableUtils.toList(roomRepository.findAll());
    }

    private Room getRoomIfExists(long id) {
        return roomRepository
                .findById(id)
                .orElse(null);
    }
}
