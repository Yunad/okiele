package okielegym.demo.service;

import okielegym.demo.persistence.model.Exercise;
import okielegym.demo.persistence.model.type.ExerciseType;
import okielegym.demo.repository.ExerciseRepository;
import okielegym.demo.service.exceptions.ServicesException;
import okielegym.demo.service.utils.ServiceUtils;
import org.apache.commons.collections4.IterableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ExerciseService implements MainService<Exercise> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExerciseService.class);
    private final ExerciseRepository exerciseRepository;

    public ExerciseService(ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    @Override
    public Exercise create(Exercise exercise) throws ServicesException {
        Exercise exerciseByExerciseType = getExerciseByExerciseType(exercise.getExerciseType());
        if (exerciseByExerciseType != null) {
            throw new ServicesException(ServiceUtils.getEntityInDatabaseMessage(exercise.getExerciseType().name()));
        }
        LOGGER.info("Saving {} to database", exercise.getExerciseType());
        return exerciseRepository.save(exercise);
    }

    @Override
    public Exercise update(Exercise exerciseToUpdate) throws ServicesException {
        Optional<Exercise> optionalExercise = exerciseRepository.findById(exerciseToUpdate.getId());
        Exercise exercise = optionalExercise.orElseThrow(ServicesException::new);

        BeanUtils.copyProperties(exerciseToUpdate, exercise);
        LOGGER.info("Updating {} in the database.", exerciseToUpdate);
        return exerciseRepository.save(exercise);
    }

    @Override
    public void delete(Exercise exercise) throws ServicesException {
        Exercise exerciseByExerciseType = getExerciseByExerciseType(exercise.getExerciseType());
        if (exerciseByExerciseType == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(exercise.getExerciseType().name()));
        }
        LOGGER.info("Deleting {} from the database.", exercise.getExerciseType());
        exerciseRepository.delete(exercise);
    }

    @Override
    public List<Exercise> getAll() {
        return IterableUtils.toList(exerciseRepository.findAll());
    }

    private Exercise getExerciseByExerciseType(ExerciseType exerciseType) {
        return exerciseRepository.findExerciseByExerciseType(exerciseType);
    }
}
