package okielegym.demo.service;

import okielegym.demo.persistence.model.User;
import okielegym.demo.persistence.model.UserType;
import okielegym.demo.service.exceptions.ServicesException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserServiceImpl userService;

    public UserDetailsServiceImpl(UserServiceImpl userService) {
        this.userService = userService;
    }

    /**
     * loads users by Email. Which is unique per user.
     *
     * @param userEmail unique
     * @return UserDetails with granted authorities
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userEmail) {
        try {
            User userByEmail = userService.getUserByEmail(userEmail);
            return buildUserForAuthentication(userByEmail, getUserAuthority(userByEmail.getUserType()));
        } catch (ServicesException e) {
            throw new UsernameNotFoundException(userEmail);
        }
    }

    private GrantedAuthority getUserAuthority(UserType userType) {
        return new SimpleGrantedAuthority(userType.toString());
    }

    private UserDetails buildUserForAuthentication(User user, GrantedAuthority authorities) {
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                user.isBlocked(), true, true, true, Collections.singleton(authorities));
    }
}
