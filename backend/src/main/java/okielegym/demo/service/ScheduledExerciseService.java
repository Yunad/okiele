package okielegym.demo.service;

import okielegym.demo.persistence.model.Room;
import okielegym.demo.persistence.model.ScheduledExercise;
import okielegym.demo.repository.ScheduledExercisesRepository;
import okielegym.demo.service.exceptions.ServicesException;
import okielegym.demo.service.utils.ServiceUtils;
import org.apache.commons.collections4.IterableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Optional;

@Service
public class ScheduledExerciseService implements MainService<ScheduledExercise> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledExerciseService.class);

    private final ScheduledExercisesRepository scheduledExercisesRepository;

    public ScheduledExerciseService(ScheduledExercisesRepository scheduledExercisesRepository) {
        this.scheduledExercisesRepository = scheduledExercisesRepository;
    }

    @Override
    public ScheduledExercise create(ScheduledExercise scheduledExercise) throws ServicesException {
        if (scheduledExercise.getId() != null) {
            throw new ServicesException(ServiceUtils.getEntityInDatabaseMessage(scheduledExercise.toString()));
        }
        LOGGER.info("Saving {} to database.", scheduledExercise);
        return scheduledExercisesRepository.save(scheduledExercise);
    }

    @Override
    public ScheduledExercise update(ScheduledExercise scheduledExercise) throws ServicesException {
        Optional<ScheduledExercise> optionalScheduledExercise = scheduledExercisesRepository.findById(scheduledExercise.getId());
        ScheduledExercise scheduledExerciseFromDatabase = optionalScheduledExercise.orElseThrow(ServicesException::new);
        BeanUtils.copyProperties(scheduledExercise, scheduledExerciseFromDatabase);
        LOGGER.info("Updating {} in database.", scheduledExerciseFromDatabase);
        return scheduledExercisesRepository.save(scheduledExerciseFromDatabase);
    }

    @Override
    public void delete(ScheduledExercise scheduledExercise) throws ServicesException {
        Optional<ScheduledExercise> optionalScheduledExercise = scheduledExercisesRepository.findById(scheduledExercise.getId());
        ScheduledExercise scheduledExerciseFromDatabase = optionalScheduledExercise.orElseThrow(ServicesException::new);
        LOGGER.info("Removing scheduledExerciseFromDatabase");
        scheduledExercisesRepository.delete(scheduledExerciseFromDatabase);
    }

    @Override
    public List<ScheduledExercise> getAll() {
        return IterableUtils.toList(scheduledExercisesRepository.findAll());
    }

    public List<ScheduledExercise> getCalendarByDayOfWeek(DayOfWeek dayOfWeek) {
        return scheduledExercisesRepository.getByDayOfWeek(dayOfWeek);
    }

    public List<ScheduledExercise> getCalendarForRoomByDayOfWeek(DayOfWeek dayOfWeek, Room room) {
        return scheduledExercisesRepository.getAllByDayOfWeekAndRoom(dayOfWeek, room);
    }

    public void refresh() {
        scheduledExercisesRepository.flush();
    }

    public void deleteById(long id) throws ServicesException {
        ScheduledExercise scheduleByID = getScheduleByID(id);
        LOGGER.info("Deleting schedule: {}", scheduleByID);
        scheduledExercisesRepository.delete(scheduleByID);
    }

    private ScheduledExercise getScheduleByID(long id) throws ServicesException {
        return scheduledExercisesRepository.findById(id)
                .orElseThrow(ServicesException::new);
    }
}
