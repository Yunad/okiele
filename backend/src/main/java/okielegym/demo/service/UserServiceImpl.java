package okielegym.demo.service;

import okielegym.demo.persistence.model.User;
import okielegym.demo.repository.UserRepository;
import okielegym.demo.repository.UserRepositoryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends UserService<User> {
    private final UserRepositoryImpl userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl(UserRepositoryImpl userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    protected UserRepository<User, Long> getRepository() {
        return this.userRepository;
    }

    @Override
    protected BCryptPasswordEncoder getBCryptPassword() {
        return this.bCryptPasswordEncoder;
    }
}
