package okielegym.demo.service;

import okielegym.demo.persistence.model.Information;
import okielegym.demo.repository.InformationRepository;
import okielegym.demo.service.exceptions.ServicesException;
import okielegym.demo.service.utils.ServiceUtils;
import org.apache.commons.collections4.IterableUtils;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class InformationService implements MainService<Information> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InformationService.class);
    private final InformationRepository informationRepository;

    public InformationService(InformationRepository informationRepository) {
        this.informationRepository = informationRepository;
    }

    @Override
    public Information create(Information information) {
        if (information.getId() != null) {
            throw new ServiceException(ServiceUtils.getEntityInDatabaseMessage(information.getInformationTypes().name()));
        }
        LOGGER.info("Saving {} to the database", information);
        return informationRepository.save(information);
    }

    @Override
    public Information update(Information information) throws ServicesException {
        Information informationById = getInformationById(information.getId());
        LOGGER.info("Updating {} in the database.", information);
        BeanUtils.copyProperties(information, informationById);
        return informationRepository.save(informationById);
    }

    @Override
    public void delete(Information information) throws ServicesException {
        Information informationById = getInformationById(information.getId());
        LOGGER.info("Deleting {} from the database.", information);
        informationRepository.delete(informationById);
    }

    public void deleteInformationById(long id) throws ServicesException {
        Information informationById = getInformationById(id);
        LOGGER.info("Deleting {} from the database.", informationById);
        informationRepository.deleteById(id);
    }

    @Override
    public List<Information> getAll() {
        return IterableUtils.toList(informationRepository.findAll());
    }

    public void refresh() {
        informationRepository.flush();
    }

    private Information getInformationById(Long id) throws ServicesException {
        return informationRepository.findById(id)
                .orElseThrow(ServicesException::new);
    }
}
