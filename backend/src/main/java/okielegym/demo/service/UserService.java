package okielegym.demo.service;

import okielegym.demo.persistence.model.User;
import okielegym.demo.persistence.model.UserType;
import okielegym.demo.repository.UserRepository;
import okielegym.demo.service.exceptions.ServicesException;
import okielegym.demo.service.utils.ServiceUtils;
import org.apache.commons.collections4.IterableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

public abstract class UserService<T extends User> implements MainService<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    protected abstract UserRepository<T, Long> getRepository();

    protected abstract BCryptPasswordEncoder getBCryptPassword();

    @Override
    public T create(T user) throws ServicesException {
        T clientByEmail = getRepository().findUserByEmail(user.getEmail());
        if (clientByEmail != null) {
            throw new ServicesException(ServiceUtils.getEntityInDatabaseMessage(user.getEmail()));
        }
        LOGGER.info("Saving {} to the database.", user);
        user.setPassword(getBCryptPassword().encode(user.getPassword()));
        return getRepository().save(user);
    }

    @Override
    public T update(@NotNull T user) throws ServicesException {
        T clientByEmail = getRepository().findUserByEmail(user.getEmail());
        if (clientByEmail == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(user.getEmail()));
        }
        BeanUtils.copyProperties(user, clientByEmail);
        LOGGER.info("Updating {} in the database.", clientByEmail);
        return getRepository().save(clientByEmail);
    }

    @Override
    public void delete(@NotNull T user) throws ServicesException {
        T clientByEmail = getRepository().findUserByEmail(user.getEmail());
        if (clientByEmail == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(user.getEmail()));
        }
        getRepository().delete(user);
    }

    @Override
    public List<T> getAll() {
        return IterableUtils.toList(getRepository().findAll());
    }

    public T getUserByEmail(@NotNull String email) throws ServicesException {
        T userByEmail = getRepository().findUserByEmail(email);
        if (userByEmail == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(email));
        }
        return userByEmail;
    }

    @Transactional
    public void deleteUserByEmail(String clientEmail) throws ServicesException {
        T userByEmail = getRepository().findUserByEmail(clientEmail);
        if (userByEmail == null) {
            throw new ServicesException(ServiceUtils.getEntityNotInTheDatabase(clientEmail));
        }
        LOGGER.info("Deleting user {}", userByEmail);
        getRepository().deleteByEmail(clientEmail);
    }

    public List<UserType> getUserTypes() {
        return getRepository().getUserTypes();
    }
}
