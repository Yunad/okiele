package okielegym.demo.service.exceptions;

public class ServicesException extends Exception {
    public ServicesException() {
    }

    public ServicesException(String message) {
        super(message);
    }
}
