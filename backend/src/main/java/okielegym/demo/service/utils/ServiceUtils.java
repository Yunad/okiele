package okielegym.demo.service.utils;

public final class ServiceUtils {
    private ServiceUtils() {
    }

    public static String getEntityNotInTheDatabase(String entityName) {
        return gymDatabaseMessageTemplate(entityName, "is not");
    }

    public static String getEntityInDatabaseMessage(String entityName) {
        return gymDatabaseMessageTemplate(entityName, "is in");
    }

    private static String gymDatabaseMessageTemplate(String entityName, String message) {
        return String.format("%s %s the database.", entityName, message);
    }

}
