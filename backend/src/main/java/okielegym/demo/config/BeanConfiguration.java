package okielegym.demo.config;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
public class BeanConfiguration {
    private static final String JDBC_DRIVER_CLASS_NAME = "org.postgresql.Driver";
    private static final String JDBC_USERNAME = "postgres";
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/okielegym";
    @Value("${spring.datasource.password}")
    private String jdbcPassword;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Primary
    public DataSource dataSource() {
        DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
        return dataSourceBuilder
                .driverClassName(JDBC_DRIVER_CLASS_NAME)
                .password(jdbcPassword)
                .username(JDBC_USERNAME)
                .url(JDBC_URL)
                .build();
    }

    @Bean
    @Profile("test")
    public DataSource testDataSource() {
        DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
        return dataSourceBuilder
                .driverClassName("org.h2.Driver")
                .password("")
                .username("sa")
                .url("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1")
                .build();
    }
}
