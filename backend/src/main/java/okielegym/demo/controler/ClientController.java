package okielegym.demo.controler;

import okielegym.demo.controler.dto.ClientDto;
import okielegym.demo.persistence.model.Client;
import okielegym.demo.service.ClientService;
import okielegym.demo.service.exceptions.ServicesException;
import org.apache.commons.collections4.IterableUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/users/clients")
public class ClientController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);
    private static final String CLIENT_AGREEMENTS_URL = "agreements/";
    private static final String CLIENT_AGREED_SMS_URL = CLIENT_AGREEMENTS_URL + "sms/{clientEmail}/{isAgreed}";
    private static final String CLIENTS_AGREED_SMS_URL = CLIENT_AGREEMENTS_URL + "sms/{isAgreed}";
    private static final String CLIENT_AGREED_EMAIL_URL = CLIENT_AGREEMENTS_URL + "email/{clientEmail}/{isAgreed}";
    private static final String CLIENTS_AGREED_EMAIL_URL = CLIENT_AGREEMENTS_URL + "email/{isAgreed}";
    private final ClientService clientService;
    private final ModelMapper modelMapper;

    public ClientController(ClientService clientService, ModelMapper modelMapper) {
        this.clientService = clientService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(name = "getClients")
    public ResponseEntity getClients() {
        List<Client> clients = IterableUtils.toList(clientService.getAll());
        List<ClientDto> clientDtoList = new ArrayList<>();
        clients.forEach(client -> clientDtoList.add(modelMapper.map(client, ClientDto.class)));
        return ResponseEntity.ok(clientDtoList);
    }

    @PostMapping(name = "createClient")
    public ResponseEntity createClient(@RequestBody ClientDto clientDto) {
        try {
            LOGGER.info("Client from Front: {}", clientDto);
            clientService.create(modelMapper.map(clientDto, Client.class));
            return ResponseEntity.status(HttpStatus.CREATED).body(clientDto);
        } catch (ServicesException e) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("Client already exists.");
        }
    }


    @PutMapping(name = "updateClient")
    public ResponseEntity updateClient(@RequestBody ClientDto clientDto) {
        try {
            Client client = modelMapper.map(clientDto, Client.class);
            Client updatedClient = clientService.update(client);
            return ResponseEntity.
                    status(HttpStatus.ACCEPTED).body(modelMapper.map(updatedClient, ClientDto.class));
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping(name = "getClientsAgreedToSms", path = CLIENTS_AGREED_SMS_URL)
    public ResponseEntity getAgreedSmsClients(@PathVariable String isAgreed) {
        List<Client> agreedSmsClients = clientService.getAgreedSmsClients(Boolean.parseBoolean(isAgreed));
        List<ClientDto> agreedEmailClientsDto = new ArrayList<>();
        agreedSmsClients.forEach(client ->
                agreedEmailClientsDto.add(modelMapper.map(client, ClientDto.class)));
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(agreedEmailClientsDto);
    }

    @PatchMapping(name = "updateSmsAgreementForClient", path = CLIENT_AGREED_SMS_URL)
    public ResponseEntity updateClientSmsAgreementByEmail(@PathVariable String clientEmail,
                                                          @PathVariable boolean isAgreed) {
        try {
            clientService.updateClientSmsAgreement(clientEmail, isAgreed);
            return ResponseEntity.ok(String.format("Updated SMS agreemend for user: %s", clientEmail));
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Client was not found");
        }
    }

    @GetMapping(name = "getClientsAgreedToEmail", path = CLIENTS_AGREED_EMAIL_URL)
    public ResponseEntity<List<ClientDto>> getAgreedEmailClients(@PathVariable String isAgreed) {
        List<Client> agreedEmailClients = clientService.getAgreedEmailClients(Boolean.parseBoolean(isAgreed));
        List<ClientDto> agreedEmailClientsDto = new ArrayList<>();
        agreedEmailClients.forEach(client -> agreedEmailClientsDto.add(modelMapper.map(client, ClientDto.class)));
        return new ResponseEntity<>(agreedEmailClientsDto, HttpStatus.OK);
    }

    @PatchMapping(name = "updateEmailAgreementForClient", path = CLIENT_AGREED_EMAIL_URL)
    public ResponseEntity updateClientEmailAgreementByEmail(@PathVariable String clientEmail,
                                                            @PathVariable boolean isAgreed) {
        try {
            clientService.updateClientEmailAgreement(clientEmail, isAgreed);
            return ResponseEntity.ok().build();
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Client not found");
        }
    }

    @DeleteMapping(name = "deleteUser", path = "/{clientEmail}")
    public ResponseEntity deleteClientByEmail(@PathVariable String clientEmail) {
        try {
            clientService.deleteUserByEmail(clientEmail);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Client has been deleted");
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Client not found.");
        }
    }
}
