package okielegym.demo.controler;

import okielegym.demo.controler.dto.ScheduleExerciseDto;
import okielegym.demo.persistence.model.ScheduledExercise;
import okielegym.demo.service.ScheduledExerciseService;
import okielegym.demo.service.exceptions.ServicesException;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/schedule/")
public class ScheduledExerciseController {
    private final ScheduledExerciseService scheduledExerciseService;
    private final ModelMapper modelMapper;

    public ScheduledExerciseController(ScheduledExerciseService scheduledExerciseService, ModelMapper modelMapper) {
        this.scheduledExerciseService = scheduledExerciseService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(name = "getCalendarsByDayWeek", path = "/{dayOfWeek}")
    public List<ScheduleExerciseDto> getScheduleByDayOfWeek(@PathVariable DayOfWeek dayOfWeek) {
        List<ScheduleExerciseDto> calendarDtoList = new ArrayList<>();
        List<ScheduledExercise> calendarByDayOfWeek = scheduledExerciseService.getCalendarByDayOfWeek(dayOfWeek);
        calendarByDayOfWeek.forEach(calendar -> calendarDtoList.add(modelMapper.map(calendar, ScheduleExerciseDto.class)));
        return calendarDtoList;
    }

    @PostMapping(name = "addSchedule", path = "/")
    public ResponseEntity addScheduleToGym(ScheduleExerciseDto scheduleExerciseDto) {
        ScheduledExercise scheduledExercise = modelMapper.map(scheduleExerciseDto, ScheduledExercise.class);
        try {
            scheduledExerciseService.create(scheduledExercise);
            return ResponseEntity.status(HttpStatus.CREATED).body(scheduledExercise);
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(name = "updateSchedule", consumes = MediaType.APPLICATION_JSON_VALUE, path = "/")
    public ResponseEntity updateSchedule(ScheduleExerciseDto scheduleExerciseDto) {
        ScheduledExercise scheduledExercise = modelMapper.map(scheduleExerciseDto, ScheduledExercise.class);
        try {
            scheduledExerciseService.update(scheduledExercise);
            return ResponseEntity.accepted().build();
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(name = "deleteSchedule", path = "/{id}")
    public ResponseEntity deleteScheduleById(@PathVariable long id) {
        try {
            scheduledExerciseService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
