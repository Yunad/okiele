package okielegym.demo.controler;

import okielegym.demo.controler.dto.InformationDto;
import okielegym.demo.persistence.model.Information;
import okielegym.demo.service.InformationService;
import okielegym.demo.service.exceptions.ServicesException;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/information/")
public class InformationController {
    private final InformationService informationService;
    private final ModelMapper modelMapper;

    public InformationController(InformationService informationService, ModelMapper modelMapper) {
        this.informationService = informationService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(name = "getAllInformation", path = "/")
    public ResponseEntity getAllInformationDto() {
        List<InformationDto> allInformationDto = new ArrayList<>();
        List<Information> allInformation = informationService.getAll();
        allInformation.forEach(information -> allInformationDto.add(modelMapper.map(information, InformationDto.class)));
        return ResponseEntity.ok(allInformationDto);
    }

    @PostMapping(name = "addInformation", path = "/")
    public ResponseEntity<InformationDto> addInformation(InformationDto informationDto) {
        Information information = modelMapper.map(informationDto, Information.class);
        Information createdInformation = informationService.create(information);
        InformationDto mappedInformation = modelMapper.map(createdInformation, InformationDto.class);
        return ResponseEntity.status(HttpStatus.CREATED).body(mappedInformation);
    }

    @PutMapping(name = " updateInformation", path = "/")
    public ResponseEntity updateInformation(InformationDto informationDto) {
        Information mappedInformation = modelMapper.map(informationDto, Information.class);
        try {
            Information updatedInformation = informationService.update(mappedInformation);
            InformationDto mappedInformationAfterUpdate = modelMapper.map(updatedInformation, InformationDto.class);
            return ResponseEntity.status(HttpStatus.CREATED).body(mappedInformationAfterUpdate);
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }

    }

    @DeleteMapping(name = "deleteInformation", path = "/{id}")
    public ResponseEntity deleteInformation(@PathVariable long id) {
        try {
            informationService.deleteInformationById(id);
            return ResponseEntity.ok().build();
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
