package okielegym.demo.controler.dto;

import okielegym.demo.persistence.model.Gym;
import okielegym.demo.persistence.model.type.InformationTypes;

import java.util.List;

public class InformationDto {
    private long id;
    private InformationTypes informationTypes;
    private String message;
    private Byte[] image;
    private List<Gym> gymInformation;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public InformationTypes getInformationTypes() {
        return informationTypes;
    }

    public void setInformationTypes(InformationTypes informationTypes) {
        this.informationTypes = informationTypes;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public List<Gym> getGymInformation() {
        return gymInformation;
    }

    public void setGymInformation(List<Gym> gymInformation) {
        this.gymInformation = gymInformation;
    }
}
