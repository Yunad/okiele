package okielegym.demo.controler.dto;

import okielegym.demo.persistence.model.Exercise;
import okielegym.demo.persistence.model.Schedule;
import okielegym.demo.persistence.model.type.RoomTypes;

import java.util.ArrayList;
import java.util.List;

public class RoomDto {
    private Long id;
    private String roomName;
    private RoomTypes roomType;
    private long capacity;
    private List<Exercise> gymClasses = new ArrayList<>();
    private List<Schedule> schedules;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public RoomTypes getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomTypes roomType) {
        this.roomType = roomType;
    }

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }

    public List<Exercise> getGymClasses() {
        return gymClasses;
    }

    public void setGymClasses(List<Exercise> gymClasses) {
        this.gymClasses = gymClasses;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }
}
