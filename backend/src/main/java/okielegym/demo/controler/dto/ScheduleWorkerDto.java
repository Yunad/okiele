package okielegym.demo.controler.dto;

import okielegym.demo.persistence.model.Worker;

import java.sql.Time;
import java.util.Date;

public class ScheduleWorkerDto {
    private long id;
    private Date date;
    private Time startingTime;
    private Time endingTime;
    private Worker worker;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(Time startingTime) {
        this.startingTime = startingTime;
    }

    public Time getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(Time endingTime) {
        this.endingTime = endingTime;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }
}
