package okielegym.demo.controler.dto;

import okielegym.demo.persistence.model.Schedule;
import okielegym.demo.persistence.model.UserType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClientDto {
    private long id;
    private String name;
    private String lastName;
    private String address;
    private Byte[] picture;
    private String email;
    private String phone;
    private String password;
    private UserType userType;
    private int absenceCounter;
    private boolean blocked;
    private boolean agreedForSms;
    private boolean agreedForEmail;
    private List<Schedule> scheduledExercises = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isAgreedForSms() {
        return agreedForSms;
    }

    public void setAgreedForSms(boolean agreedForSms) {
        this.agreedForSms = agreedForSms;
    }

    public boolean isAgreedForEmail() {
        return agreedForEmail;
    }

    public void setAgreedForEmail(boolean agreedForEmail) {
        this.agreedForEmail = agreedForEmail;
    }

    public int getAbsenceCounter() {
        return absenceCounter;
    }

    public void setAbsenceCounter(int absenceCounter) {
        this.absenceCounter = absenceCounter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Byte[] getPicture() {
        return picture;
    }

    public void setPicture(Byte[] picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public List<Schedule> getScheduledExercises() {
        return scheduledExercises;
    }

    public void setScheduledExercises(List<Schedule> scheduledExercises) {
        this.scheduledExercises = scheduledExercises;
    }

    @Override
    public String toString() {
        return "ClientDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", picture=" + Arrays.toString(picture) +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", userType=" + userType +
                ", absenceCounter=" + absenceCounter +
                ", isBlocked=" + blocked +
                ", isAgreedForSms=" + agreedForSms +
                ", isAgreedForEmail=" + agreedForEmail +
                ", scheduledExercises=" + scheduledExercises +
                '}';
    }
}
