package okielegym.demo.controler.dto;

import okielegym.demo.persistence.model.Information;
import okielegym.demo.persistence.model.ScheduledExercise;

import java.sql.Time;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GymDto {
    private String name;
    private String address;
    private boolean parkingLot;
    private boolean sauna;
    private Time startingHours;
    private Time endingHours;
    private Set<Information> gymInformation = new HashSet<>();
    private List<ScheduledExercise> scheduledExercises = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isParkingLot() {
        return parkingLot;
    }

    public void setParkingLot(boolean parkingLot) {
        this.parkingLot = parkingLot;
    }

    public boolean isSauna() {
        return sauna;
    }

    public void setSauna(boolean sauna) {
        this.sauna = sauna;
    }

    public Time getStartingHours() {
        return startingHours;
    }

    public void setStartingHours(Time startingHours) {
        this.startingHours = startingHours;
    }

    public Time getEndingHours() {
        return endingHours;
    }

    public void setEndingHours(Time endingHours) {
        this.endingHours = endingHours;
    }

    public Set<Information> getGymInformation() {
        return gymInformation;
    }

    public void setGymInformation(Set<Information> gymInformation) {
        this.gymInformation = gymInformation;
    }

    public List<ScheduledExercise> getScheduledExercises() {
        return scheduledExercises;
    }

    public void setScheduledExercises(List<ScheduledExercise> scheduledExercises) {
        this.scheduledExercises = scheduledExercises;
    }
}
