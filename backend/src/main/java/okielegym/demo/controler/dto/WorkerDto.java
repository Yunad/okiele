package okielegym.demo.controler.dto;

import okielegym.demo.persistence.model.ScheduledExercise;
import okielegym.demo.persistence.model.UserType;
import okielegym.demo.persistence.model.WorkerSchedule;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WorkerDto {
    private long id;
    private String name;
    private String lastName;
    private String address;
    private Byte[] picture;
    private String email;
    private String phone;
    private String password;
    private UserType userType;
    private Set<ScheduledExercise> scheduledExercises = new HashSet<>();
    private BigDecimal salary;
    private List<WorkerSchedule> workersSchedule = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Byte[] getPicture() {
        return picture;
    }

    public void setPicture(Byte[] picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Set<ScheduledExercise> getScheduledExercises() {
        return scheduledExercises;
    }

    public void setScheduledExercises(Set<ScheduledExercise> scheduledExercises) {
        this.scheduledExercises = scheduledExercises;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public List<WorkerSchedule> getWorkersSchedule() {
        return workersSchedule;
    }

    public void setWorkersSchedule(List<WorkerSchedule> workersSchedule) {
        this.workersSchedule = workersSchedule;
    }
}
