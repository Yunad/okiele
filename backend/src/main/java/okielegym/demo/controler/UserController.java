package okielegym.demo.controler;

import okielegym.demo.controler.dto.UserAuthenticationDTO;
import okielegym.demo.persistence.model.User;
import okielegym.demo.service.UserServiceImpl;
import okielegym.demo.service.exceptions.ServicesException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users/")
public class UserController {
    private final UserServiceImpl userService;
    private final BCryptPasswordEncoder bCrpBCryptPasswordEncoder;

    public UserController(UserServiceImpl userService, BCryptPasswordEncoder bCrpBCryptPasswordEncoder) {
        this.userService = userService;
        this.bCrpBCryptPasswordEncoder = bCrpBCryptPasswordEncoder;
    }

    @PostMapping(path = "/sing-up")
    public ResponseEntity signUp(@RequestBody UserAuthenticationDTO userAuthenticationDTO) {
        try {
            User userByEmail = userService.getUserByEmail(userAuthenticationDTO.getEmail());
            userByEmail.setPassword(bCrpBCryptPasswordEncoder.encode(userByEmail.getPassword()));
            userService.update(userByEmail);
            return ResponseEntity.accepted().body("accepted signup method.");
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @GetMapping(path = "/types")
    public ResponseEntity getUserTypes() {
        return ResponseEntity.ok(userService.getUserTypes());
    }
}
