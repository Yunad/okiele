package okielegym.demo.controler;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
public class Test {
    @GetMapping(name = "getClients", path = "/external")
    public ResponseEntity getClients() {
        return ResponseEntity.ok("RESPONSE FFS");
    }
}
