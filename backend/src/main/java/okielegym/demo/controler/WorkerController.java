package okielegym.demo.controler;

import okielegym.demo.controler.dto.ScheduleWorkerDto;
import okielegym.demo.controler.dto.WorkerDto;
import okielegym.demo.persistence.model.Worker;
import okielegym.demo.persistence.model.WorkerSchedule;
import okielegym.demo.service.WorkerService;
import okielegym.demo.service.exceptions.ServicesException;
import org.apache.commons.collections4.IterableUtils;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/users/workers")
public class WorkerController {
    private static final String WORKER_CALENDAR_URL = "/calendar/{workerEmail}";
    private final WorkerService workerService;
    private final ModelMapper modelMapper;

    public WorkerController(WorkerService workerService, ModelMapper modelMapper) {
        this.workerService = workerService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(name = "getAllWorkers")
    public ResponseEntity<List<WorkerDto>> getAllWorkers() {
        List<WorkerDto> workerListDto = new ArrayList<>();
        List<Worker> workers = IterableUtils.toList(workerService.getAll());
        workers.forEach(worker -> workerListDto.add(modelMapper.map(worker, WorkerDto.class)));
        return ResponseEntity.ok(workerListDto);
    }

    @GetMapping(name = "getWorker", path = "/{workerEmail}")
    public ResponseEntity<WorkerDto> getWorkerCalendar(@PathVariable String workerEmail) {
        try {
            Worker worker = workerService.getUserByEmail(workerEmail);
            WorkerDto workerDto = modelMapper.map(worker, WorkerDto.class);
            return ResponseEntity.ok(workerDto);
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(name = "updateWorker")
    public ResponseEntity updateWorker(@RequestBody WorkerDto workerDto) {
        Worker mappedWorker = modelMapper.map(workerDto, Worker.class);
        try {
            WorkerDto remappedWorker = modelMapper.map(workerService.update(mappedWorker), WorkerDto.class);
            return ResponseEntity.accepted().body(remappedWorker);
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(name = "createWorker")
    public ResponseEntity<WorkerDto> createWorker(@RequestBody WorkerDto workerDto) {
        Worker mappedWorker = modelMapper.map(workerDto, Worker.class);
        try {
            Worker workerAfterCreation = workerService.create(mappedWorker);
            WorkerDto mappedWorkerDto = modelMapper.map(workerAfterCreation, WorkerDto.class);
            return ResponseEntity.status(HttpStatus.CREATED).body(mappedWorkerDto);
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(name = "getWorkerCalendar", path = WORKER_CALENDAR_URL)
    public ResponseEntity getWorkerCalendarList(@PathVariable String workerEmail) {
        List<ScheduleWorkerDto> workerCalendarDtoList = new ArrayList<>();
        try {
            Set<WorkerSchedule> workerScheduleList = workerService.getWorkerCalendarList(workerEmail);
            workerScheduleList
                    .forEach(workerCalendar -> workerCalendarDtoList.add(modelMapper.map(workerCalendar, ScheduleWorkerDto.class)));
            modelMapper.map(workerScheduleList, ScheduleWorkerDto.class);
            return ResponseEntity.ok(workerCalendarDtoList);
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(name = "addWorkerCalendar", path = WORKER_CALENDAR_URL)
    public ResponseEntity<Void> addWorkerCalendar(@PathVariable String workerEmail,
                                                  @RequestBody ScheduleWorkerDto scheduleWorkerDto) {
        WorkerSchedule workerSchedule = modelMapper.map(scheduleWorkerDto, WorkerSchedule.class);
        try {
            workerService.addScheduleToWorker(workerEmail, workerSchedule);
            return ResponseEntity.accepted().build();
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(name = "updateWorkerCalendar", path = WORKER_CALENDAR_URL)
    public ResponseEntity<Void> updateWorkerCalendar(@PathVariable String workerEmail,
                                                     @RequestBody ScheduleWorkerDto scheduleWorkerDto) {
        WorkerSchedule workerSchedule = modelMapper.map(scheduleWorkerDto, WorkerSchedule.class);
        try {
            workerService.updateWorkerCalendar(workerEmail, workerSchedule);
            return ResponseEntity.accepted().build();
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
