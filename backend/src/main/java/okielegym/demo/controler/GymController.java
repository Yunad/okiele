package okielegym.demo.controler;

import okielegym.demo.controler.dto.GymDto;
import okielegym.demo.controler.dto.InformationDto;
import okielegym.demo.controler.dto.ScheduleExerciseDto;
import okielegym.demo.persistence.model.Gym;
import okielegym.demo.persistence.model.Information;
import okielegym.demo.persistence.model.ScheduledExercise;
import okielegym.demo.service.GymService;
import okielegym.demo.service.exceptions.ServicesException;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/gym/")
public class GymController {
    private static final String GYM_CALENDAR_URL = "/calendar/{gymName}";
    private static final String GYM_INFORMATION_URL = "/information/{gymName}";
    private final GymService gymService;
    private final ModelMapper modelMapper;

    public GymController(GymService gymService, ModelMapper modelMapper) {
        this.gymService = gymService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(name = "getGyms", path = "/")
    public ResponseEntity getAllGyms() {
        List<Gym> gyms = gymService.getAll();
        List<GymDto> gymsDto = new ArrayList<>();
        gyms.forEach(gym -> gymsDto.add(modelMapper.map(gym, GymDto.class)));
        return new ResponseEntity<>(gymsDto, HttpStatus.OK);
    }

    @GetMapping(name = "getGymByName", path = "/{gymName}")
    public ResponseEntity getGymByName(@PathVariable String gymName) {
        Gym gymByName = gymService.getGymByName(gymName);
        GymDto mappedGym = modelMapper.map(gymByName, GymDto.class);
        return ResponseEntity.ok(mappedGym);
    }

    @PostMapping(name = "createGym", path = "/")
    public ResponseEntity createGym(@RequestBody GymDto gymDto) {
        try {
            Gym gym = modelMapper.map(gymDto, Gym.class);
            Gym createdGym = gymService.create(gym);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdGym);
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Gym with that name already exists");
        }
    }

    @DeleteMapping(name = "removeGym", path = "/{id}")
    public ResponseEntity deleteGymById(@PathVariable long id) {
        try {
            gymService.deleteGymById(id);
            return ResponseEntity.ok().build();
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(name = "updateGym", value = "/")
    public ResponseEntity updateGym(@RequestBody GymDto gymDto) {
        try {
            Gym gymToUpdate = modelMapper.map(gymDto, Gym.class);
            gymService.update(gymToUpdate);
            return ResponseEntity.accepted().build();
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    @PostMapping(name = "addCalendarToGym", path = GYM_CALENDAR_URL)
    public ResponseEntity createGymCalendar(@PathVariable String gymName,
                                            @RequestBody ScheduleExerciseDto scheduleExerciseDto) {
        try {
            ScheduledExercise scheduledExercise = modelMapper.map(scheduleExerciseDto, ScheduledExercise.class);
            gymService.createScheduledExercise(gymName, scheduledExercise);
            return ResponseEntity.ok().build();
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot update schedule exercise. Gym not found");
        }
    }

    @PutMapping(name = "updateGymCalendar", path = GYM_CALENDAR_URL)
    public ResponseEntity updateGymCalendar(@PathVariable String gymName,
                                            @RequestBody ScheduleExerciseDto scheduleExerciseDto) {
        ScheduledExercise scheduledExercise = modelMapper.map(scheduleExerciseDto, ScheduledExercise.class);
        try {
            Gym gymAfterUpdate = gymService.updateScheduledExercise(gymName, scheduledExercise);
            return ResponseEntity.accepted().body(gymAfterUpdate);
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot update Calendar. Gym not found");
        }
    }

    @DeleteMapping(name = "removeCalendarFromGym", path = GYM_CALENDAR_URL)
    public ResponseEntity deleteGymCalendar(@PathVariable String gymName,
                                            @RequestBody ScheduleExerciseDto scheduleExerciseDto) {
        ScheduledExercise scheduledExercise = modelMapper.map(scheduleExerciseDto, ScheduledExercise.class);
        try {
            Gym gymAfterRemove = gymService.deleteScheduledExercise(gymName, scheduledExercise);
            return ResponseEntity.ok(gymAfterRemove);
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Gym  not found");
        }
    }

    @GetMapping(name = "getGymInformationList", path = GYM_INFORMATION_URL)
    public ResponseEntity getGymInformation(@PathVariable String gymName) {
        List<InformationDto> getGymInformationList = new ArrayList<>();
        try {
            Set<Information> gymInformationList = gymService.getGymInformationList(gymName);
            gymInformationList.forEach(information -> getGymInformationList.add(modelMapper.map(information,
                    InformationDto.class)));
            return ResponseEntity.ok(getGymInformationList);
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot get information. Gym not found");
        }
    }

    @PostMapping(name = "addGymInformation", path = GYM_INFORMATION_URL)
    public ResponseEntity createGymInformation(@RequestParam String gymName,
                                               @RequestBody InformationDto informationDto) {
        try {
            Information information = modelMapper.map(informationDto, Information.class);
            gymService.createGymInformation(gymName, information);
            return ResponseEntity.accepted().build();
        } catch (ServicesException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot create information. Gym not found.");
        }
    }

    @PutMapping(name = "updateGymInformation", path = GYM_INFORMATION_URL)
    public ResponseEntity updateGymInformation(@PathVariable String gymName, InformationDto informationDto) {
        try {
            Information information = modelMapper.map(informationDto, Information.class);
            gymService.updateGymInformation(gymName, information);
            return ResponseEntity.accepted().body("Gym success updated");
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(name = "removeGymInformation", path = GYM_INFORMATION_URL)
    public ResponseEntity deleteGymInformation(@PathVariable String gymName,
                                               @RequestBody InformationDto informationDto) {
        try {
            Information information = modelMapper.map(informationDto, Information.class);
            Gym gymAfterUpdate = gymService.deleteGymInformation(gymName, information);
            return ResponseEntity.ok(gymAfterUpdate);
        } catch (ServicesException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
