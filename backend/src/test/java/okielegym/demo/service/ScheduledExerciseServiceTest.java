package okielegym.demo.service;

import okielegym.demo.TestConst;
import okielegym.demo.TestUtils;
import okielegym.demo.persistence.model.ScheduledExercise;
import okielegym.demo.repository.ScheduledExercisesRepository;
import okielegym.demo.service.exceptions.ServicesException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@ActiveProfiles("test")
class ScheduledExerciseServiceTest extends TestConst {

    @Autowired
    private ScheduledExercisesRepository scheduledExercisesRepository;
    @Autowired
    private ScheduledExerciseService scheduledExerciseService;

    @BeforeEach
    void setUp() {
        ScheduledExercise scheduledExercise = TestUtils.prepareScheduledExercise();
        scheduledExercisesRepository.save(scheduledExercise);
    }

    @AfterEach
    void tearDown() {
        scheduledExercisesRepository.deleteAll();
    }

    @Test
    void create() throws ServicesException {
        ScheduledExercise scheduledExercise = TestUtils.prepareScheduledExercise();
        scheduledExercise.setDayOfWeek(DayOfWeek.SUNDAY);

        ScheduledExercise scheduledExerciseFromDatabase = scheduledExerciseService.create(scheduledExercise);

        Assertions.assertNotNull(scheduledExerciseFromDatabase.getId());
    }

    @Test
    void update() throws ServicesException {
        List<ScheduledExercise> scheduledExerciseList = scheduledExercisesRepository.findAll();
        ScheduledExercise scheduledExercise = scheduledExerciseList.stream().findFirst().orElseThrow();
        scheduledExercise.setDayOfWeek(DayOfWeek.WEDNESDAY);

        scheduledExerciseService.update(scheduledExercise);

        ScheduledExercise scheduledExerciseAfterUpdate =
                scheduledExercisesRepository.findById(scheduledExercise.getId()).orElseThrow();
        Assertions.assertEquals(DayOfWeek.WEDNESDAY, scheduledExerciseAfterUpdate.getDayOfWeek());
    }

    @Test
    void delete() throws ServicesException {
        List<ScheduledExercise> scheduledExerciseList = scheduledExercisesRepository.findAll();
        ScheduledExercise scheduledExercise = scheduledExerciseList.stream().findFirst().orElseThrow();

        scheduledExerciseService.delete(scheduledExercise);

        Optional<ScheduledExercise> optionalScheduledExercise = scheduledExercisesRepository.findById(scheduledExercise.getId());
        Assertions.assertTrue(optionalScheduledExercise.isEmpty());
    }

    @Test
    void getAll() {
        List<ScheduledExercise> all = scheduledExerciseService.getAll();

        Assertions.assertFalse(all.isEmpty());
    }

    @Test
    void getCalendarByDayOfWeek() {
        List<ScheduledExercise> calendarByDayOfWeek = scheduledExerciseService.getCalendarByDayOfWeek(DayOfWeek.MONDAY);

        Assertions.assertFalse(calendarByDayOfWeek.isEmpty());
    }

    @Test
    void getCalendarForRoomByDayOfWeek() {
        ScheduledExercise first = scheduledExercisesRepository.findAll().stream().findFirst().orElseThrow();
        List<ScheduledExercise> calendarForRoomByDayOfWeek = scheduledExerciseService.getCalendarForRoomByDayOfWeek(first.getDayOfWeek(), first.getRoom());
        System.out.println(calendarForRoomByDayOfWeek);

        Assertions.assertFalse(calendarForRoomByDayOfWeek.isEmpty());
    }
}
