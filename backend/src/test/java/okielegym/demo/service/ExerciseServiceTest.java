package okielegym.demo.service;

import okielegym.demo.TestConst;
import okielegym.demo.TestUtils;
import okielegym.demo.persistence.model.Exercise;
import okielegym.demo.persistence.model.type.ExerciseType;
import okielegym.demo.repository.ExerciseRepository;
import okielegym.demo.service.exceptions.ServicesException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

@SpringBootTest
@ActiveProfiles("test")
class ExerciseServiceTest extends TestConst {
    @Autowired
    private ExerciseRepository exerciseRepository;
    @Autowired
    private ExerciseService exerciseService;

    @AfterEach
    public void tearDown() {
        exerciseRepository.deleteAll();
    }

    @BeforeEach
    public void setup() {
        Exercise exercise = TestUtils.prepareExercise();
        exerciseRepository.save(exercise);
    }

    @Test
    void create() throws ServicesException {
        Exercise exerciseForTest = TestUtils.prepareExercise();
        exerciseForTest.setExerciseType(ExerciseType.CARDIO);

        exerciseService.create(exerciseForTest);

        Exercise exerciseByExerciseType = exerciseRepository.findExerciseByExerciseType(ExerciseType.CARDIO);
        Assertions.assertNotNull(exerciseByExerciseType, "Could not find Exercise by Cardio Type.");
    }

    @Test
    void update() throws ServicesException {
        List<Exercise> exercises = exerciseRepository.findAll();
        Exercise exerciseToUpdate = exercises.stream().findFirst().orElseThrow();
        exerciseToUpdate.setExerciseType(ExerciseType.HEALTHY);

        exerciseService.update(exerciseToUpdate);

        Exercise exercise = exerciseRepository.findById(exerciseToUpdate.getId()).orElseThrow();
        Assertions.assertEquals(ExerciseType.HEALTHY, exercise.getExerciseType());
    }

    @Test
    void delete() throws ServicesException {
        List<Exercise> exercises = exerciseRepository.findAll();
        Exercise exercise = exercises.stream().findFirst().orElseThrow();

        exerciseService.delete(exercise);

        Optional<Exercise> optionalExercise = exerciseRepository.findById(exercise.getId());
        Assertions.assertFalse(optionalExercise.isPresent(), "Did not remove exercise from database.");
    }

    @Test
    void getAll() {
        exerciseRepository.save(TestUtils.prepareExercise());

        List<Exercise> all = exerciseService.getAll();

        Assertions.assertEquals(2, all.size());
    }
}
