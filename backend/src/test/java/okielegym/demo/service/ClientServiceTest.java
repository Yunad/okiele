package okielegym.demo.service;

import okielegym.demo.TestConst;
import okielegym.demo.TestUtils;
import okielegym.demo.persistence.model.Client;
import okielegym.demo.repository.ClientRepository;
import okielegym.demo.service.exceptions.ServicesException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

@SpringBootTest
@ActiveProfiles("test")
class ClientServiceTest extends TestConst {
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientService clientService;

    @BeforeEach
    void prepareClientInDatabase() {
        Client client = TestUtils.prepareClient();

        clientRepository.save(client);
    }


    @AfterEach
    void tearDown() {
        clientRepository.deleteAll();
    }

    @Test
    void create() throws ServicesException {
        Client clientToTest = TestUtils.prepareClient();
        clientToTest.setEmail(getEmailTestString());

        clientService.create(clientToTest);

        Client userByEmail = clientRepository.findUserByEmail(getEmailTestString());
        Assertions.assertEquals(clientToTest.getEmail(), userByEmail.getEmail());
    }

    @Test
    void throwsExceptionWhenCreatingUserWithExistingEmail() {
        Client clientToTest = TestUtils.prepareClient();

        Assertions.assertThrows(ServicesException.class, () -> clientService.create(clientToTest));
    }

    @Test
    void update() throws ServicesException {
        Client client = clientRepository.findUserByEmail(getTestEmail());
        String updatedName = "updatedName";
        client.setName(updatedName);

        Client clientAfterUpdate = clientService.update(client);

        Assertions.assertEquals(client.getId(), clientAfterUpdate.getId());
        Assertions.assertEquals(clientAfterUpdate.getName(), updatedName);
    }

    @Test
    void delete() throws ServicesException {
        Client userByEmail = clientRepository.findUserByEmail(getTestEmail());

        clientService.delete(userByEmail);

        Client userByEmailAfterDelete = clientRepository.findUserByEmail(getTestEmail());
        Assertions.assertNull(userByEmailAfterDelete);
    }

    @Test
    void getAll() {
        addTwoAnotherClientsToDb();
        List<Client> clients = clientService.getAll();

        Assertions.assertEquals(3, clients.size());
    }

    @Test
    void getUserByEmail() throws ServicesException {
        Client userByEmail = clientService.getUserByEmail(getTestEmail());

        Assertions.assertEquals(getTestEmail(), userByEmail.getEmail());
    }

    @Test
    void getAgreedSmsClients() {
        Assertions.assertFalse(clientService.getAgreedSmsClients(true).isEmpty());
    }

    @Test
    void updateClientSmsAgreement() throws ServicesException {
        Client clientForTest = TestUtils.prepareClient();
        clientForTest.setEmail(getEmailTestString());
        clientForTest.setAgreedForSms(false);
        clientRepository.save(clientForTest);

        Client clientAfterUpdate = clientService.updateClientSmsAgreement(getEmailTestString(), true);

        Assertions.assertTrue(clientAfterUpdate.isAgreedForSms());
    }

    @Test
    void getAgreedEmailClients() {
        Assertions.assertTrue(clientService.getAgreedEmailClients(true).isEmpty());
    }

    @Test
    void updateClientEmailAgreement() throws ServicesException {
        Client clientForTest = TestUtils.prepareClient();
        clientForTest.setEmail(getEmailTestString());
        clientForTest.setAgreedForEmail(true);
        clientRepository.save(clientForTest);

        Client clientAfterUpdate = clientService.updateClientEmailAgreement(getEmailTestString(), false);

        Assertions.assertFalse(clientAfterUpdate.isAgreedForEmail());
    }


    private void addTwoAnotherClientsToDb() {
        Client client = TestUtils.prepareClient();
        Client anotherClient = TestUtils.prepareClient();
        client.setEmail("2ndEmail@test.test");

        clientRepository.save(client);
        clientRepository.save(anotherClient);
    }
}
