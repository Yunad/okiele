package okielegym.demo.service;

import okielegym.demo.TestConst;
import okielegym.demo.TestUtils;
import okielegym.demo.persistence.model.Gym;
import okielegym.demo.persistence.model.Information;
import okielegym.demo.persistence.model.ScheduledExercise;
import okielegym.demo.persistence.model.type.InformationTypes;
import okielegym.demo.repository.GymRepository;
import okielegym.demo.service.exceptions.ServicesException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.DayOfWeek;
import java.util.Set;

@SpringBootTest
@ActiveProfiles("test")
class GymServiceTest extends TestConst {
    private final String gymNameUpdate = "gymNameUpdate";
    @Autowired
    private GymRepository gymRepository;
    @Autowired
    private GymService gymService;

    @AfterEach
    void tearDown() {
        gymRepository.deleteAll();
    }

    @BeforeEach
    void persistDefaultFullySetupGym() {
        Gym gym = TestUtils.prepareGymWithAllInformation();
        gymRepository.save(gym);
    }

    @Test
    void create() throws ServicesException {
        Gym gym = TestUtils.prepareSoloGym();
        gym.setName(gymNameUpdate);

        gymService.create(gym);

        Gym gymByName = gymRepository.findGymByName(gymNameUpdate);
        Assertions.assertNotNull(gymByName);
    }

    @Test
    void update() throws ServicesException {
        String anotherTestAddress = "anotherTestAddress";
        Gym gymByName = getGymIfExists(getGymName());
        gymByName.setAddress(anotherTestAddress);

        gymService.update(gymByName);

        Gym gymAfterUpdate = getGymIfExists(gymByName.getName());
        Assertions.assertEquals(anotherTestAddress, gymAfterUpdate.getAddress());

    }

    @Test
    void delete() throws ServicesException {
        Gym gymByName = getGymIfExists(getGymName());

        gymService.delete(gymByName);

        Assertions.assertNull(gymRepository.findGymByName(getGymName()));
    }

    @Test
    void getGymByName() {
        Assertions.assertNotNull(gymService.getGymByName(getGymName()));
    }

    @Test
    void deleteGymInformation() throws ServicesException {
        Gym gymIfExists = getGymIfExists(getGymName());
        Set<Information> informationList = gymIfExists.getInformationList();
        Assertions.assertFalse(informationList.isEmpty());

        gymService.deleteGymInformation(gymIfExists.getName(), informationList.stream().findFirst().get());

        Gym gymByName = gymRepository.findGymByName(gymIfExists.getName());
        Assertions.assertTrue(gymByName.getInformationList().isEmpty());
    }

    @Test
    void createGymInformation() throws ServicesException {
        Gym gymIfExists = getGymIfExists(getGymName());
        Information informationToUpdate = TestUtils.prepareInformation();
        informationToUpdate.setInformationTypes(InformationTypes.BLOG);

        Gym gymAfterUpdate = gymService.createGymInformation(gymIfExists.getName(), informationToUpdate);

        Set<Information> informationList = gymAfterUpdate.getInformationList();
        Information filteredInformation = informationList
                .stream()
                .filter(information -> information.getInformationTypes().equals(InformationTypes.BLOG))
                .findFirst()
                .orElseThrow();
        Assertions.assertNotNull(filteredInformation);
    }

    @Test
    void updateGymInformation() throws ServicesException {
        Gym gymIfExists = getGymIfExists(getGymName());
        Information informationToUpdate = gymIfExists.getInformationList()
                .stream()
                .findFirst()
                .orElse(null);
        Assertions.assertNotNull(informationToUpdate);
        informationToUpdate.setInformationTypes(InformationTypes.BLOG);

        Gym gymAfterUpdate = gymService.updateGymInformation(getGymName(), informationToUpdate);

        Information updatedInformation = gymAfterUpdate.getInformationList()
                .stream()
                .filter(information -> information.getInformationTypes().equals(InformationTypes.BLOG))
                .findFirst().orElseThrow();
        Assertions.assertNotNull(updatedInformation);
    }

    @Test
    void getGymInformationList() throws ServicesException {
        Set<Information> gymInformationList = gymService.getGymInformationList(getGymName());

        Assertions.assertFalse(gymInformationList.isEmpty());
    }

    @Test
    void createGymScheduledExercise() throws ServicesException {
        ScheduledExercise scheduledExercise = TestUtils.prepareScheduledExercise();

        Gym gymFromDatabase = gymService.createScheduledExercise(getGymName(), scheduledExercise);

        Assertions.assertFalse(gymFromDatabase.getScheduledExercises().isEmpty());
    }

    @Test
    void deleteGymSchedule() throws ServicesException {
        Gym gymIfExists = getGymIfExists(getGymName());
        Set<ScheduledExercise> scheduledExercises = gymIfExists.getScheduledExercises();

        Gym gymAfterDeletion = gymService
                .deleteScheduledExercise(gymIfExists.getName(), scheduledExercises.stream().findFirst().get());

        Assertions.assertTrue(gymAfterDeletion.getScheduledExercises().isEmpty());
    }

    @Test
    void getGymSchedule() throws ServicesException {
        Set<ScheduledExercise> gymScheduledExerciseList = gymService.getGymScheduledExerciseList(getGymName());

        Assertions.assertFalse(gymScheduledExerciseList.isEmpty());
    }

    @Test
    void updateGymScheduledExercise() throws ServicesException {
        Gym gymIfExists = getGymIfExists(getGymName());
        Set<ScheduledExercise> scheduledExercises = gymIfExists.getScheduledExercises();
        ScheduledExercise scheduledExercise = scheduledExercises.stream().findAny().get();
        Assertions.assertNotNull(scheduledExercise);
        scheduledExercise.setDayOfWeek(DayOfWeek.SATURDAY);

        Gym gymAfterUpdate = gymService.updateScheduledExercise(gymIfExists.getName(), scheduledExercise);

        ScheduledExercise exerciseAfterUpdate = gymAfterUpdate.getScheduledExercises().stream().findFirst().get();
        Assertions.assertEquals(DayOfWeek.SATURDAY, exerciseAfterUpdate.getDayOfWeek());
    }

    private Gym getGymIfExists(String gymName) {
        return gymRepository.findGymByName(gymName);
    }
}
