package okielegym.demo.service;

import okielegym.demo.TestConst;
import okielegym.demo.TestUtils;
import okielegym.demo.persistence.model.Information;
import okielegym.demo.persistence.model.type.InformationTypes;
import okielegym.demo.repository.GymRepository;
import okielegym.demo.repository.InformationRepository;
import okielegym.demo.service.exceptions.ServicesException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

@SpringBootTest
@ActiveProfiles("test")
class InformationServiceTest extends TestConst {

    @Autowired
    private InformationRepository informationRepository;
    @Autowired
    private InformationService informationService;
    @Autowired
    private GymRepository gymRepository;

    @BeforeEach
    void setUp() {
        Information information = TestUtils.prepareInformation();
        informationRepository.save(information);
    }

    @AfterEach
    void tearDown() {
        informationRepository.deleteAll();
        gymRepository.deleteAll();
    }

    @Test
    void create() {
        Information information = TestUtils.prepareInformation();

        Information createdInformation = informationRepository.save(information);

        Assertions.assertNotNull(createdInformation.getId(), "Could not add element to database.");
    }

    @Test
    void update() throws ServicesException {
        List<Information> all = informationRepository.findAll();
        Information informationToUpdate = all.stream().findFirst().orElseThrow();
        informationToUpdate.setInformationTypes(InformationTypes.IMPORTANT);

        Information informationAfterUpdate = informationService.update(informationToUpdate);

        Assertions.assertEquals(InformationTypes.IMPORTANT, informationAfterUpdate.getInformationTypes());
    }

    @Test
    void delete() throws ServicesException {
        List<Information> informationList = informationRepository.findAll();
        Information informationToDelete = informationList.stream().findFirst().orElseThrow();

        informationService.delete(informationToDelete);

        Optional<Information> informationById = informationRepository.findById(informationToDelete.getId());
        Assertions.assertTrue(informationById.isEmpty());
    }

    @Test
    void getAll() {
        List<Information> informationList = informationService.getAll();

        Assertions.assertFalse(informationList.isEmpty());
    }
}
