package okielegym.demo;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TestConst {
    private static final String TEST_NAME = "testName";
    private static final String TEST_LAST_NAME = "testLastName";
    private static final String TEST_ADDRESS = "testAddress";
    private static final String TEST_PHONE = "testPhone";
    private static final String TEST_EMAIL = "testEmail@email.email";
    private static final String TEST_PASSWORD = "testPassword";
    private static final int TEST_ABSENCE_COUNTER = 10;
    private static final boolean IS_AGREED_FOR_SMS = true;
    private static final boolean IS_NOT_AGREED_FOR_EMAIL = false;
    private static final boolean IS_BLOCKED = true;
    private static final double TEST_SALARY = 2000.1234;
    private static final String GYM_NAME = "GymName";
    private static final String GYM_ADDRESS = "Test Address";
    private static final Time GYM_STARTING_HOURS = Time.valueOf("8:00:00");
    private static final Time GYM_ENDING_HOURS = Time.valueOf("20:00:00");
    private static final String ROOM_NAME = "TestName";
    private static final int ROOM_CAPACITY = 23;
    private static final String INFORMATION_MESSAGE = "Information message";
    private static final Date DATE_TODAY = Date.valueOf(LocalDate.now());
    private static final DayOfWeek DAY_OF_WEEK_MONDAY = DayOfWeek.MONDAY;
    private static final Time CURRENT_TIME_MINUS_TWO_HOURS = Time.valueOf(LocalTime.now().minusHours(2));
    private static final Time CURRENT_TIME = Time.valueOf(LocalTime.now());
    private static final Timestamp CURRENT_TIME_TIMESTAMP = Timestamp.valueOf(LocalDateTime.now());
    private static final String EMAIL_TEST_STRING = "emailForTesting@test.test";

    public static String getTestName() {
        return TEST_NAME;
    }

    public static String getTestLastName() {
        return TEST_LAST_NAME;
    }

    public static String getTestAddress() {
        return TEST_ADDRESS;
    }

    public static String getTestPhone() {
        return TEST_PHONE;
    }

    public static String getTestEmail() {
        return TEST_EMAIL;
    }

    public static String getTestPassword() {
        return TEST_PASSWORD;
    }

    public static int getTestAbsenceCounter() {
        return TEST_ABSENCE_COUNTER;
    }

    public static boolean isIsAgreedForSms() {
        return IS_AGREED_FOR_SMS;
    }

    public static boolean isIsNotAgreedForEmail() {
        return IS_NOT_AGREED_FOR_EMAIL;
    }

    public static boolean isBlocked() {
        return IS_BLOCKED;
    }

    public static double getTestSalary() {
        return TEST_SALARY;
    }

    public static String getGymName() {
        return GYM_NAME;
    }

    public static String getGymAddress() {
        return GYM_ADDRESS;
    }

    public static Time getGymStartingHours() {
        return GYM_STARTING_HOURS;
    }

    public static Time getGymEndingHours() {
        return GYM_ENDING_HOURS;
    }

    public static String getRoomName() {
        return ROOM_NAME;
    }

    public static int getRoomCapacity() {
        return ROOM_CAPACITY;
    }

    public static String getInformationMessage() {
        return INFORMATION_MESSAGE;
    }

    public static Date getDateToday() {
        return DATE_TODAY;
    }

    public static DayOfWeek getDayOfWeekMonday() {
        return DAY_OF_WEEK_MONDAY;
    }

    public static Time getCurrentTimeMinusTwoHours() {
        return CURRENT_TIME_MINUS_TWO_HOURS;
    }

    public static Time getCurrentTime() {
        return CURRENT_TIME;
    }

    public static Timestamp getCurrentTimeTimestamp() {
        return CURRENT_TIME_TIMESTAMP;
    }

    public static String getEmailTestString() {
        return EMAIL_TEST_STRING;
    }
}
