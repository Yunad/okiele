package okielegym.demo;

import okielegym.demo.persistence.model.Client;
import okielegym.demo.persistence.model.Exercise;
import okielegym.demo.persistence.model.Gym;
import okielegym.demo.persistence.model.Information;
import okielegym.demo.persistence.model.Room;
import okielegym.demo.persistence.model.ScheduledExercise;
import okielegym.demo.persistence.model.UserType;
import okielegym.demo.persistence.model.Worker;
import okielegym.demo.persistence.model.WorkerSchedule;
import okielegym.demo.persistence.model.builder.ClientBuilder;
import okielegym.demo.persistence.model.builder.WorkerBuilder;
import okielegym.demo.persistence.model.type.ExerciseType;
import okielegym.demo.persistence.model.type.InformationTypes;
import okielegym.demo.persistence.model.type.RoomTypes;
import okielegym.demo.persistence.model.type.UserTypes;

import java.math.BigDecimal;

public final class TestUtils extends TestConst {

    private TestUtils() {
    }

    public static Client prepareClient() {
        ClientBuilder clientBuilder = new ClientBuilder();
        clientBuilder
                .isAgreedForSms(isIsAgreedForSms())
                .isAgreedForEmail(isIsNotAgreedForEmail())
                .isBlocked(isBlocked())
                .setAbsenceCounter(getTestAbsenceCounter())
                .setName(getTestName())
                .setLastName(getTestLastName())
                .setUserType(new UserType(UserTypes.ADMINISTRATOR.name()))
                .setAddress(getGymAddress())
                .setEmail(getTestEmail())
                .setPhone(getTestPhone())
                .setPassword(getTestPassword());
        return clientBuilder.build();
    }

    public static Worker prepareWorker() {
        WorkerBuilder WorkerBuilder = new WorkerBuilder();
        WorkerBuilder
                .setSalary(BigDecimal.valueOf(getTestSalary()))
                .setWorkerSchedule(prepareWorkerSchedule())
                .setName(getTestName())
                .setLastName(getTestLastName())
                .setAddress(getTestAddress())
                .setEmail(getTestEmail())
                .setPhone(getTestPhone())
                .setUserType(new UserType(UserTypes.WORKER.name()))
                .setPassword(getTestPassword());

        return WorkerBuilder.build();
    }

    public static Gym prepareSoloGym() {
        Gym gym = new Gym();
        gym.setName(getGymName());
        gym.setAddress(getTestAddress());
        gym.setStartingHours(getGymStartingHours());
        gym.setEndingHours(getGymEndingHours());
        gym.setParkingLot(false);
        gym.setSauna(true);

        return gym;
    }

    public static Information prepareInformation() {
        Information information = new Information();
        information.setInformationTypes(InformationTypes.IMPORTANT);
        information.setMessage(getInformationMessage());

        return information;
    }

    public static ScheduledExercise prepareScheduledExercise() {
        ScheduledExercise scheduledExercise = new ScheduledExercise();
        scheduledExercise.setTimestamp(getCurrentTimeTimestamp());
        scheduledExercise.setTime(getCurrentTime());
        scheduledExercise.setDayOfWeek(getDayOfWeekMonday());
        scheduledExercise.setWorker(prepareWorker());
        scheduledExercise.addClient(prepareClient());
        scheduledExercise.setRoom(prepareRoom());

        return scheduledExercise;
    }

    public static Room prepareRoom() {
        return new Room(RoomTypes.JUMPIT, getRoomCapacity(), getRoomName());
    }

    public static Gym prepareGymWithAllInformation() {
        Gym gym = prepareSoloGym();
        gym.addScheduledExercise(prepareScheduledExercise());
        gym.addInformation(prepareInformation());

        return gym;
    }

    public static WorkerSchedule prepareWorkerSchedule() {
        WorkerSchedule workerSchedule = new WorkerSchedule();
        workerSchedule.setDate(getDateToday());
        workerSchedule.setStartingTime(getCurrentTimeMinusTwoHours());
        workerSchedule.setEndingTime(getCurrentTime());

        return workerSchedule;
    }

    public static Exercise prepareExercise() {
        Exercise exercise = new Exercise();
        exercise.setRoom(prepareRoom());
        exercise.setExerciseType(ExerciseType.DYNAMIC);

        return exercise;
    }
}
