// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const API_URL = 'http://localhost/api';

export const environment = {
    production: false,
    clientsApiUrl: API_URL.concat('/users/clients/'),
    workersApiUrl: API_URL.concat('/users/workers/'),
    informationApiUrl: API_URL.concat('/information/'),
    gymApiUrl: API_URL.concat('/gym/'),
    scheduledExerciseUrl: API_URL.concat('/schedule/exercises/'),
    authenticationUrl: API_URL.concat('/users/')
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
