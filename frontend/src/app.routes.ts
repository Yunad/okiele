import {RouterModule, Routes} from '@angular/router';
import {ExerciseScheduleComponent} from './app/components/exercise-schedule/exercise-schedule.component';
import {WorkerScheduleComponent} from './app/components/worker-schedule/worker-schedule.component';
import {ClientComponent} from './app/components/client/client.component';
import {WorkerComponent} from './app/components/worker/worker.component';
import {InformationComponent} from './app/components/information/information.component';
import {AdminComponent} from './app/components/admin/admin.component';
import {ContactComponent} from './app/components/contact/contact.component';
import {UserComponent} from './app/user/user.component';
import {AuthGuard} from './app/auth.guard';
import {ClientFormComponent} from './app/components/client/client-form/client-form.component';
import {ClientEmailComponent} from './app/components/client/client-email/client-email.component';
import {ClientListComponent} from './app/components/client/client-list/client-list.component';
import {ExternalApiComponent} from './app/external-api/external-api.component';

export const routes: Routes = [
    {path: 'contact', component: ContactComponent},
    {path: 'schedule', component: ExerciseScheduleComponent},
    {path: 'profile', component: UserComponent, canActivate: [AuthGuard]},
    {
        path: 'external-api',
        component: ExternalApiComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'admin', component: AdminComponent, canActivate: [AuthGuard], children: [
            {path: 'exercise-schedule', component: ExerciseScheduleComponent},
            {path: 'worker-schedule', component: WorkerScheduleComponent},
            {
                path: 'client', component: ClientComponent, children: [
                    {path: 'client-form', component: ClientFormComponent},
                    {path: 'client-list', component: ClientListComponent},
                    {path: 'client-email', component: ClientEmailComponent}
                ]
            },
            {path: 'worker', component: WorkerComponent},
            {path: 'information', component: InformationComponent}
        ]
    },
];

export const appRoutingProviders: any[] = [];

export const routing = RouterModule.forRoot(routes);
