import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FooterComponent} from './components/footer/footer.component';
import {HomeComponent} from './components/home/home.component';
import {ContactComponent} from './components/contact/contact.component';
import {HeaderComponent} from './components/header/header.component';
import {AdminComponent} from './components/admin/admin.component';
import {ExerciseScheduleComponent} from './components/exercise-schedule/exercise-schedule.component';
import {WorkerScheduleComponent} from './components/worker-schedule/worker-schedule.component';
import {ClientComponent} from './components/client/client.component';
import {WorkerComponent} from './components/worker/worker.component';
import {InformationComponent} from './components/information/information.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {appRoutingProviders, routing} from '../app.routes';
import {PromoComponent} from './promo/promo.component';
import {UserComponent} from './user/user.component';
import {ProfileComponent} from './components/profile/profile.component';
import {ClientEmailComponent} from './components/client/client-email/client-email.component';
import {ClientListComponent} from './components/client/client-list/client-list.component';
import {ClientFormComponent} from './components/client/client-form/client-form.component';
import {authInterceptorProviders} from './service/src/auth-interceptor';
import {ExternalApiComponent} from './external-api/external-api.component';

@NgModule({
    declarations: [
        AppComponent,
        FooterComponent,
        HomeComponent,
        ContactComponent,
        HeaderComponent,
        AdminComponent,
        ExerciseScheduleComponent,
        WorkerScheduleComponent,
        ClientComponent,
        ClientEmailComponent,
        ClientListComponent,
        ClientFormComponent,
        WorkerComponent,
        InformationComponent,
        PromoComponent,
        UserComponent,
        ProfileComponent,
        ExternalApiComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule,
        routing
    ],
    providers: [appRoutingProviders, authInterceptorProviders],
    bootstrap: [AppComponent]
})
export class AppModule {
}
