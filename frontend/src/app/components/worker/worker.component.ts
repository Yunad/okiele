import {Component, OnInit} from '@angular/core';
import {WorkerService} from '../../service/src/worker.service';
import {OkieleGymError} from '../../service/src/error/OkieleGymError';
import {Worker} from '../../backend-types/Worker';
import {WorkerSchedule} from '../../backend-types/WorkerSchedule';

@Component({
    selector: 'app-worker',
    templateUrl: './worker.component.html',
    styleUrls: ['./worker.component.css']
})
export class WorkerComponent implements OnInit {
    workers: Worker[];
    worker: Worker;
    workerSchedule: WorkerSchedule;
    email: string;

    constructor(private workerService: WorkerService) {
    }

    ngOnInit() {
    }

    public createWorker() {
        this.workerService.createWorker(this.worker)
            .subscribe(value => console.log(value),
                error => OkieleGymError.handleError(error));
    }

    public getWorkers() {
        this.workerService.getWorkers()
            .subscribe(resWorker => this.workers = resWorker,
                error => OkieleGymError.handleError(error));
    }

    public getWorkerByEmail() {
        this.workerService.getWorkerByEmail(this.email)
            .subscribe(resWorker => this.worker = resWorker,
                error => OkieleGymError.handleError(error));
    }

    public deleteWorkerById() {
        this.workerService.deleteWorkerById(this.worker.id)
            .subscribe(
                value => console.log(value),
                error => OkieleGymError.handleError(error));
    }

    public addWorkerSchedule() {
        this.workerService.addWorkerScheduleByEmail(this.worker.email, this.workerSchedule)
            .subscribe(
                value => console.log(value),
                error => OkieleGymError.handleError(error));
    }

    public updateWorkerSchedule() {
        this.workerService.updateWorkerScheduleByEmail(this.worker.email, this.workerSchedule)
            .subscribe(
                value => console.log(value),
                error => OkieleGymError.handleError(error)
            );
    }

    public deleteWorkerSchedule() {
        this.workerService.deleteWorkerSchedule(this.worker.id, this.workerSchedule.id)
            .subscribe(
                value => console.log(value),
                error => OkieleGymError.handleError(error)
            );
    }
}
