import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../service/src/client.service';
import {Client} from '../../backend-types/Client';
import {Observable} from 'rxjs';
import {OkieleGymError} from '../../service/src/error/OkieleGymError';

@Component({
    selector: 'app-client',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

    private clients: Client[];
    private client: Client = new Client();
    private test$: Observable<Client[]>;

    constructor(private clientService: ClientService) {
    }

    ngOnInit() {
        this.clients = [];
    }

    public getAllClients(): any {
        this.test$ = this.clientService.getClients();
        this.clientService.getClients()
            .subscribe(wantedClients => this.clients = wantedClients);
    }

    public addClient(): any {
        return this.clientService.createClient(this.client)
            .subscribe(
                value => console.log(value),
                error => {
                    OkieleGymError.handleError(error);
                },
                () => {
                    console.log('Success');
                }
            );
    }

    public updateClient(client: Client): any {
        this.clientService.updateClient(client)
            .subscribe(value => console.log(value),
                error => {
                    OkieleGymError.handleError(error);
                },
                () => console.log('Success'));
    }

    public deleteClientByEmail(email: string): any {
        this.clientService.deleteClientByEmail(email)
            .subscribe(value => console.log(value),
                error => {
                    OkieleGymError.handleError(error);
                },
                () => console.log('Success'));
    }

    public getClientsAgreedToEmail(agreed: boolean): any {
        this.clientService.getClientsAgreedToSms(agreed)
            .subscribe(
                clients => this.clients = clients,
                error => {
                    OkieleGymError.handleError(error);
                },
                () => console.log('Success'));
    }

    public getClientsAgreedToSms(agreed: boolean): any {
        this.clientService.getClientsAgreedToSms(agreed)
            .subscribe(
                clients => this.clients = clients,
                error => OkieleGymError.handleError(error),
                () => console.log('success'));
    }
}
