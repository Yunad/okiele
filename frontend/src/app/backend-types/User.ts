// TODO change string into picture representation
import {UserType} from '../service/src/enum/UserType';

export class User {
    id: number;
    name: string;
    lastName: string;
    address: string;
    picture: string;
    email: string;
    phone: number;
    password: string;
    userType: UserType;
    blocked: boolean;

    constructor() {
    }
}
