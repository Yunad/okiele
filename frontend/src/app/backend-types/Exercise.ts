import {Room} from './Room';
import {ExerciseType} from '../service/src/enum/ExerciseType';

export class Exercise {
    id: number;
    exerciseType: ExerciseType;
    room: Room;
}
