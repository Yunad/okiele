import {Information} from './Information';
import {ScheduledExercise} from './ScheduledExercise';
import {Time} from '@angular/common';

export class Gym {
    id: number;
    name: string;
    address: string;
    isParkingLot: boolean;
    isSauna: boolean;
    startingHours: Time;
    endingHours: Time;
    gymInformation: Information[];
    scheduledExercises: ScheduledExercise[];
}
