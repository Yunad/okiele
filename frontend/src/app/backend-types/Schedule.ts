import {Time} from '@angular/common';
import {Room} from './Room';
import {Worker} from './Worker';
import {Client} from './Client';

export class Schedule {
    id: number;
    time: Time;
    timestamp: number;
    room: Room;
    worker: Worker;
    clients: Client[];
}
