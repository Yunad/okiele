import {Worker} from './Worker';
import {Time} from '@angular/common';

export class WorkerSchedule {
    id: number;
    date: Date;
    startingTime: Time;
    endingTime: Time;
    worker: Worker;
}
