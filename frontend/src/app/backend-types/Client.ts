import {User} from './User';
import {ScheduledExercise} from './ScheduledExercise';

export class Client extends User {
    agreedForSms: boolean;
    agreedForEmail: boolean;
    absenceCounter: number;
    scheduleExercise: ScheduledExercise[];

    constructor() {
        super();
    }

}
