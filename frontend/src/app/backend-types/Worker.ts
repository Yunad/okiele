import {User} from './User';
import {WorkerSchedule} from './WorkerSchedule';
import {ScheduledExercise} from './ScheduledExercise';

export class Worker extends User {
    salary: number;
    WorkerSchedule: WorkerSchedule;
    scheduledExercise: ScheduledExercise;
}
