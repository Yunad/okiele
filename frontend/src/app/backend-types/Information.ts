import {InformationType} from '../service/src/enum/InformationType';
import {Gym} from './Gym';

export class Information {
    id: number;
    informationType: InformationType;
    message: string;
    image: string;
    gymInformation: Gym[];
}
