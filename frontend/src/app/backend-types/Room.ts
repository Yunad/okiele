import {RoomType} from '../service/src/enum/RoomType';
import {Exercise} from './Exercise';
import {Schedule} from './Schedule';

export class Room {
    id: number;
    roomName: String;
    roomType: RoomType;
    capacity: number;
    gymClasses: Exercise[];
    schedules: Schedule[];
}
