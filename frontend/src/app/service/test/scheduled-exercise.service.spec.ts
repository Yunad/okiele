import { TestBed } from '@angular/core/testing';

import { ScheduledExerciseService } from '../src/scheduled-exercise.service';

describe('ScheduledExerciseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScheduledExerciseService = TestBed.get(ScheduledExerciseService);
    expect(service).toBeTruthy();
  });
});
