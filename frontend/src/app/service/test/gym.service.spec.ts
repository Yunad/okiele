import {TestBed} from '@angular/core/testing';

import {GymService} from '../src/gym.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Gym} from '../../backend-types/Gym';
import {Time} from '@angular/common';

describe('GymService', () => {
    let gymService: GymService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [GymService],
            imports: [HttpClientTestingModule]
        });
        gymService = TestBed.inject(GymService);
        httpMock = TestBed.inject(HttpTestingController);
    });

    it('should be created', () => {
        expect(gymService).toBeTruthy();
    });

    function createGym(): Gym {
        const endingHours: Time;
        endingHours.hours = 10;
        endingHours.minutes = 40;
        const gym: Gym = new Gym();
        gym.address = 'TestAddress';
        gym.endingHours = endingHours;
        gym.isParkingLot = true;

        return gym;
    }

    it('Successfully creates gym.', () => {
        const gym = createGym();
        const gymObservable = gymService.createGym(gym);
        const subscription = gymObservable.subscribe(
            value => expect(value).toBeTrue());
    });
});
