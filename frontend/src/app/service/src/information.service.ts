import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Information} from '../../backend-types/Information';

const API_INFORMATION_URL = environment.informationApiUrl;

@Injectable({
    providedIn: 'root'
})
export class InformationService {

    constructor(private http: HttpClient) {
    }

    public getAllInformation(): Observable<Information[]> {
        return this.http.get<Information[]>(API_INFORMATION_URL);
    }

    public createInformation(information: Information): Observable<Information> {
        return this.http.post<Information>(API_INFORMATION_URL, information);
    }

    public updateInformation(information: Information): Observable<any> {
        return this.http.put<any>(API_INFORMATION_URL, information);
    }

    public deleteInformation(information: Information): Observable<any> {
        return this.http.delete(API_INFORMATION_URL.concat(String(information.id)));
    }
}
