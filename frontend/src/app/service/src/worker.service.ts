import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Worker} from '../../backend-types/Worker';
import {Schedule} from '../../backend-types/Schedule';
import {WorkerSchedule} from '../../backend-types/WorkerSchedule';

const API_WORKERS_URL = environment.workersApiUrl;
const API_WORKERS_SCHEDULE_URL = API_WORKERS_URL.concat('/schedule/');

@Injectable({
    providedIn: 'root'
})
export class WorkerService {

    constructor(private http: HttpClient) {
    }

    public getWorkers(): Observable<Worker[]> {
        return this.http.get<Worker[]>(API_WORKERS_URL);
    }

    public getWorkerByEmail(email: string): Observable<Worker> {
        return this.http.get<Worker>(API_WORKERS_URL.concat(email));
    }

    public createWorker(worker: Worker): Observable<Worker> {
        return this.http.post<Worker>(API_WORKERS_URL, worker);
    }

    public updateWorker(worker: Worker): Observable<Worker> {
        return this.http.post<Worker>(API_WORKERS_URL, worker);
    }

    public getWorkerScheduleByEmail(workerEmail: string): Observable<Schedule[]> {
        return this.http.get<Schedule[]>(API_WORKERS_SCHEDULE_URL.concat(workerEmail));
    }

    public addWorkerScheduleByEmail(workerEmail: string, workerSchedule: WorkerSchedule): Observable<any> {
        return this.http.post<any>(API_WORKERS_SCHEDULE_URL.concat(workerEmail), workerSchedule);
    }

    public updateWorkerScheduleByEmail(workerEmail: string, workerSchedule: WorkerSchedule): Observable<any> {
        return this.http.patch<any>(API_WORKERS_SCHEDULE_URL.concat(workerEmail), workerSchedule);
    }

    deleteWorkerById(id: number) {
        return this.http.delete(API_WORKERS_URL.concat('/' + id));
    }

    deleteWorkerSchedule(userId: number, scheduleId: number) {
        return this.http.delete(API_WORKERS_SCHEDULE_URL.concat('/' + userId + '/').concat('/' + scheduleId + '/'));
    }
}
