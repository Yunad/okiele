import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Client} from '../../backend-types/Client';

const API_CLIENTS_URL = environment.clientsApiUrl;
const API_AGREEMENTS_URL = API_CLIENTS_URL.concat('agreements/');
const API_AGREEMENTS_SMS_URL = API_AGREEMENTS_URL.concat('sms/');
const API_AGREEMENTS_EMAIL_URL = API_AGREEMENTS_URL.concat('email/');

@Injectable({
    providedIn: 'root'
})
export class ClientService {

    constructor(private http: HttpClient) {
    }

    public getClients(): Observable<Client[]> {
        return this.http.get <Client[]>(API_CLIENTS_URL);
    }

    public createClient(client: Client): Observable<Client> {
        const reqHeader = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http.post<Client>(API_CLIENTS_URL, client, {headers: reqHeader, reportProgress: true, responseType: 'json'});
    }

    public updateClientByEmail(user: Client): Observable<Client> {
        return this.http.post<Client>(API_CLIENTS_URL.concat(user.email), user);
    }

    public getClientsAgreedToEmail(agreed: boolean): Observable<Client[]> {
        return this.http.get<Client[]>(API_AGREEMENTS_EMAIL_URL.concat(String(agreed)));
    }

    public getClientsAgreedToSms(agreed: boolean): Observable<Client[]> {
        return this.http.get<Client[]>(API_AGREEMENTS_SMS_URL.concat(String(agreed)));
    }

    public updateClientSmsAgreementByEmail(client: Client, agreed: boolean): Observable<HttpResponse<any>> {
        return this.http.get<any>(API_AGREEMENTS_EMAIL_URL.concat(client.email).concat('/' + agreed));
    }

    public updateClientEmailAgreementByEmail(client: Client, agreed: boolean): Observable<any> {
        return this.http.get<any>(API_AGREEMENTS_EMAIL_URL.concat(client.email).concat('/' + agreed));
    }

    updateClient(client: Client): Observable<any> {
        return this.http.put<Client>(API_CLIENTS_URL, client);
    }

    deleteClientByEmail(email: string) {
        return this.http.delete<Client>(API_CLIENTS_URL.concat(email));
    }
}
