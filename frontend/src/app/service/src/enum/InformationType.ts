export enum InformationType {
    INFO,
    IMPORTANT,
    BLOG,
    PRIVATE
}
