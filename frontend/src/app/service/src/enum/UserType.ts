export enum UserType {
    ADMINISTRATOR,
    WORKER,
    USER,
}
