export enum ExerciseType {
    CARDIO,
    WEIGHTS,
    HEALTHY,
    DYNAMIC,
    STATIC
}
