import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';

export class OkieleGymError {
    public static handleError(error: HttpErrorResponse) {
        if (error.status === 409) {
            throwError(error);
        }
    }
}
