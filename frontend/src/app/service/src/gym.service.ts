import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Gym} from '../../backend-types/Gym';

const API_GYM_URL = environment.gymApiUrl;

@Injectable({
    providedIn: 'root'
})
export class GymService {

    constructor(private http: HttpClient) {
    }

    public getGyms(): Observable<Gym[]> {
        return this.http.get<Gym[]>(API_GYM_URL);
    }

    public getGymByName(gymName: string): Observable<Gym> {
        return this.http.get<Gym>(API_GYM_URL.concat(gymName));
    }

    public createGym(gym: Gym): Observable<Gym> {
        return this.http.post<Gym>(API_GYM_URL, gym);
    }

    public updateGym(gym: Gym): Observable<Gym> {
        return this.http.put<Gym>(API_GYM_URL, gym);
    }

    public deleteGymById(gym: Gym): Observable<any> {
        return this.http.delete(API_GYM_URL.concat('/' + gym.id));
    }

}
