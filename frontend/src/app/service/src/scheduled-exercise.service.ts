import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Injectable} from '@angular/core';
import {ScheduledExercise} from '../../backend-types/ScheduledExercise';
import {Observable} from 'rxjs';
import {WeekDay} from '@angular/common';

const API_SCHEDULED_EXERCISE_URL = environment.scheduledExerciseUrl;

@Injectable({
    providedIn: 'root'
})

export class ScheduledExerciseService {
    constructor(private http: HttpClient) {
    }

    public createSchedule(scheduledExercise: ScheduledExercise): Observable<ScheduledExercise> {
        return this.http.post<ScheduledExercise>(API_SCHEDULED_EXERCISE_URL, scheduledExercise);
    }

    public updateSchedule(scheduledExercise: ScheduledExercise): Observable<ScheduledExercise> {
        return this.http.put<ScheduledExercise>(API_SCHEDULED_EXERCISE_URL, scheduledExercise);
    }

    public deleteSchedule(scheduledExercise: ScheduledExercise): Observable<any> {
        return this.http.delete(API_SCHEDULED_EXERCISE_URL.concat(String(scheduledExercise.id)));
    }

    public getScheduleDetailsByDayOfWeek(dayOfWeek: WeekDay) {
        return this.http.get(API_SCHEDULED_EXERCISE_URL.concat(String(dayOfWeek)));
    }
}
